const path = require('path');
const WebpackNotifierPlugin = require('webpack-notifier');
// const sassImporter = require('node-sass-import');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

var pkg = require('./package.json');
var Name = require('@egml/utils/node/Name').default;
var name = new Name(pkg.egml.name, pkg.egml.namespace);

//	███████ ██████  ██      ██ ████████
//	██      ██   ██ ██      ██    ██
//	███████ ██████  ██      ██    ██
//	     ██ ██      ██      ██    ██
//	███████ ██      ███████ ██    ██
// 
//	 ██████ ██   ██ ██    ██ ███    ██ ██   ██ ███████
//	██      ██   ██ ██    ██ ████   ██ ██  ██  ██
//	██      ███████ ██    ██ ██ ██  ██ █████   ███████
//	██      ██   ██ ██    ██ ██  ██ ██ ██  ██       ██
//	 ██████ ██   ██  ██████  ██   ████ ██   ██ ███████
// 
//	#split #chunks
var splitChunks = {
	chunks: 'all',
	automaticNameDelimiter: '--',
	// Значения по умолчанию: https://webpack.js.org/plugins/split-chunks-plugin#optimizationsplitchunks
	// minSize: 0,
	cacheGroups: {
		// vendors: {
		// 	test: function(module, chunks) {
		// 		// console.log(module.resource, chunks[0].name == 'modal');
		// 		return (
		// 			module.resource && 
		// 			module.resource.match(/[\\/]node_modules[\\/]/) != null && 
		// 			// module.type == 'javascript/auto' && 
		// 			chunks[0].name != 'modal-assets' &&
		// 			chunks[0].name != 'authorized' &&
		// 			chunks[0].name != 'guest' &&
		// 			true
		// 		);
		// 	},
		// 	// test: /[\\/]node_modules[\\/]/,
        //   	priority: -10,
		// },
		vendors: false,
		// default: false,
		// modal: {
		// 	enforce: true,
		// 	test: /^modal$/,
		// 	// test: /([\\/]src[\\/]modal\.js$|[\\/]src[\\/]spinner\.js$|[\\/]src[\\/]viewport-cropper\.js$|[\\/]src[\\/]img[\\/]svg-sprite[\\/]cross\.svg$)/,
		// 	minSize: 0,
		// 	// priority: -15,
		// },
		// modal: {
		// 	// enforce: true,
		// 	test: 'modal',
		// 	// test: /([\\/]node_modules[\\/]|[\\/]svg-sprite[\\/].+\.svg$|[\\/]src[\\/]modal\.js$|[\\/]package\.json$|[\\/]src[\\/]common\.js$|[\\/]src[\\/]sprite\.override\.js$)/,
		// 	minSize: 0,
		// 	reuseExistingChunk: true,
		// 	priority: 10,
		// },
		// common: {
		// 	// enforce: true,
		// 	test: /([\\/]node_modules[\\/]|[\\/]svg-sprite[\\/].+\.svg$|^modal|[\\/]package\.json$|[\\/]src[\\/]common\.js$|[\\/]src[\\/]sprite\.override\.js$)/,
		// 	// test: /([\\/]node_modules[\\/]|[\\/]svg-sprite[\\/].+\.svg$|[\\/]src[\\/]modal\.js$|[\\/]package\.json$|[\\/]src[\\/]common\.js$|[\\/]src[\\/]sprite\.override\.js$)/,
		// 	minSize: 1,
		// 	reuseExistingChunk: true,
		// 	priority: 1,
		// },
	},
};

//	██████  ██    ██ ██      ███████ ███████
//	██   ██ ██    ██ ██      ██      ██
//	██████  ██    ██ ██      █████   ███████
//	██   ██ ██    ██ ██      ██           ██
//	██   ██  ██████  ███████ ███████ ███████
// 
//	#rules
var rules = [
	// Babel
	// {
	// 	test: /\.m?js$/,
	// 	use: [
	// 		{
	// 			loader: 'babel-loader',
	// 			options: {
	// 				presets: [
	// 					[
	// 						'@babel/preset-env',
	// 						{
	// 							targets: {
	// 								android: "33",
	// 							},
	// 							useBuiltIns: 'entry',
	// 							modules: false, // Позволить Webpack'у транспилировать импорты
	// 						},
	// 					],
	// 				],
	// 				plugins: [
	// 					'@babel/plugin-syntax-dynamic-import',
	// 				],
	// 			},
	// 		},
	// 	],
	// },
	{
		test: /\.scss$/,
		use: [
			'style-loader',
			'css-loader',
			{
				loader: 'sass-loader',
				options: {
					sourceMap: false,
					outputStyle: 'compressed',
					// outputStyle: 'expanded',
					indentType: 'tab',
					indentWidth: 1,
					// importer: sassImporter,
				},
			},
		],
	},
	{
		test: /\.svg$/,
		use: [
			{
				loader: 'svg-sprite-loader',
				options: {
					symbolId: name.css(['svg', 'sprite']) + '-[name]',
					// #WARN: Почему-то путь относителен директории, где находятся svg-файлы
					spriteModule: '../../sprite.override.js',
				},
			},
			{
				loader: 'svgo-loader',
				options: {
					plugins: [
						{ removeViewBox: false },
						{ removeDimensions: true },
						{ removeUselessStrokeAndFill: false },
						{ cleanupNumericValues: false },
						{ convertPathData: false },
						{ mergePaths: false },
					],
				},
			},
		],
	},
	{
		test: /\.pug$/,
		use: [
			'pug-loader',
		],
	},
];


module.exports = [

	//	██████  ██████   ██████  ██████  
	//	██   ██ ██   ██ ██    ██ ██   ██ 
	//	██████  ██████  ██    ██ ██   ██ 
	//	██      ██   ██ ██    ██ ██   ██ 
	//	██      ██   ██  ██████  ██████  
	// 
	//	#prod
	{
		name: 'prod',
		mode: 'production',
		entry: { 
			guest: './src/guest',
			authorized: './src/authorized',
		},
		output: {
			path: path.resolve(__dirname, 'dist'),
			filename: name.kebab('[name]') + '.js',
			library: [ name.ns.camel, name.base.camel ],
		},
		optimization: {
			splitChunks,
		},
		stats: 'minimal',
		plugins: [
			new WebpackNotifierPlugin(),
			// ЭТИ ПЛАГИНЫ ПЕРЕПИСЫВАЮТ ФАЙЛЫ В `./demo/`! Не подключать их, не подумав
			// new HtmlWebpackPlugin({
			// 	template: 'src/html/entry-guest.php',
			// 	inject: 'head',
			// 	chunks: [ 'guest' ],
			// 	filename: path.resolve(__dirname, 'demo/guest.php'),
			// }),
			// new HtmlWebpackPlugin({
			// 	template: 'src/html/entry-authorized.php',
			// 	inject: 'head',
			// 	chunks: [ 'authorized' ],
			// 	filename: path.resolve(__dirname, 'demo/authorized.php'),
			// }),
		],
		module: {
			rules,
		},
	},

	//	██████  ███████ ██    ██
	//	██   ██ ██      ██    ██
	//	██   ██ █████   ██    ██
	//	██   ██ ██       ██  ██
	//	██████  ███████   ████
	// 
	// 	#dev
	{
		name: 'dev',
		mode: 'development',
		entry: { 
			guest: './src/guest',
			authorized: './src/authorized',
			// test: './src/test',
		},
		output: {
			path: path.resolve(__dirname, 'demo/assets'),
			filename: '[name].js',
			library: [ name.ns.camel, name.base.camel ],
		},
		devtool: 'inline-source-map',
		optimization: {
			usedExports: true,
			splitChunks,
		},
		plugins: [
			new WebpackNotifierPlugin(),
			new HtmlWebpackPlugin({
				template: 'src/html/entry-guest.php',
				inject: 'head',
				chunks: [ 'guest' ],
				filename: path.resolve(__dirname, 'demo/guest.php'),
			}),
			new HtmlWebpackPlugin({
				template: 'src/html/entry-authorized.php',
				inject: 'head',
				chunks: [ 'authorized' ],
				filename: path.resolve(__dirname, 'demo/authorized.php'),
			}),
			// new BundleAnalyzerPlugin(),
		],
		module: {
			rules
		},
		stats: {
			hash: false,
			version: false,
			timings: false,
			builtAt: false,

			assets: true,
			// excludeAssets: [ /authorized/ ],

			entrypoints: true,
			chunkGroups: true,
			chunks: true,
			chunkModules: true,
			// chunkOrigins: true,
			chunksSort: '!size',

			cached: true,

			children: true,

			publicPath: false,

			modules: false,
			modulesSort: '!size',

			// exclude: false,

			// reasons: true,
			reasons: false,

			excludeModules: false, // Вот ключевой момент для показа скрытых модулей
			maxModules: Infinity, // И это тоже
		},
	}
];
