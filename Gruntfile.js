const sass = require('node-sass');
// const sass = require('sass'); // Выдает больше информации при ошибке, но работает в 4 и более раз медленнее
// const sassImporter = require('node-sass-import');

var Name = require('@egml/utils/node/Name').default;
var pkg = require('./package.json');
var name = new Name(pkg.egml.name, pkg.egml.namespace);

module.exports = grunt => {

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({

		pkg: pkg,
		name: name,
		
		// /*
		// ███████  █████  ███████ ███████
		// ██      ██   ██ ██      ██
		// ███████ ███████ ███████ ███████
		//      ██ ██   ██      ██      ██
		// ███████ ██   ██ ███████ ███████
		// */
		// #sass #css
		sass: {
			options: {
				implementation: sass,
				sourceMap: false,
				outputStyle: 'expanded',
				indentType: 'tab',
				indentWidth: 1,
				// importer: sassImporter,
			},
			demo: {
				files: [
					{
						cwd: 'src/css/demo',
						src: [ '**/*.scss' ],
						dest: 'demo/assets',
						rename: function(dest, src) {
							if (// если я когда-нибудь сломаю голову об это, то я не виноват...
								src == 'guest.css' ||
								src == 'authorized.css' ||
								src == 'modal.css' ||
								false
							) 
								return dest + '/' + name.kebab(src);
							else
								return dest + '/' + src;
						},
						ext: '.css',
						expand: true,
					}
				],
			},
			dist: {
				options: {
					outputStyle: 'compressed',
				},
				files: [
					{
						cwd: 'src/css/dist',
						src: [ '**/*.scss' ],
						dest: 'dist',
						rename: function(dest, src) {
							return dest + '/' + name.kebab(src);
						},
						ext: '.css',
						expand: true,
					}
				],
			},
		},

		// /*
		// ██ ███    ███  █████   ██████  ███████ ███    ███ ██ ███    ██
		// ██ ████  ████ ██   ██ ██       ██      ████  ████ ██ ████   ██
		// ██ ██ ████ ██ ███████ ██   ███ █████   ██ ████ ██ ██ ██ ██  ██
		// ██ ██  ██  ██ ██   ██ ██    ██ ██      ██  ██  ██ ██ ██  ██ ██
		// ██ ██      ██ ██   ██  ██████  ███████ ██      ██ ██ ██   ████
		// 
		//  * https://www.npmjs.com/package/grunt-contrib-imagemin
		//  * SVGO:
		//    https://github.com/sindresorhus/grunt-svgmin#available-optionsplugins
		//    https://github.com/svg/svgo/tree/master/plugins
		// */
		// #imagemin #img
		
		imagemin: {
			build: {
				files: [{
					expand: true,
					cwd: 'src/img',
					src: [ '**/*.{svg,png,jpg,gif}', '!svg-sprite/*' ],
					dest: 'demo/assets/img',
				}],
			},
			svg: {
				options: {
					svgoPlugins: [
						// Информация по плагинам здесь: http://bit.ly/2UXDwRf
						{ removeViewBox: false },
						{ removeDimensions: true },
						{ removeUselessStrokeAndFill: false },
						{ cleanupNumericValues: false },
						{ convertPathData: false },
						{ mergePaths: false },
					]
				},
				files: [{
					expand: true,
					cwd: 'src/img/svg-sprite',
					src: ['*.svg'],
					dest: 'runtime/svg-sprite',
					ext: '.optimized.svg',
				}],
			}
		},

		// /*
		// ███████ ██    ██  ██████  ███████ ████████  ██████  ██████  ███████
		// ██      ██    ██ ██       ██         ██    ██    ██ ██   ██ ██
		// ███████ ██    ██ ██   ███ ███████    ██    ██    ██ ██████  █████
		//      ██  ██  ██  ██    ██      ██    ██    ██    ██ ██   ██ ██
		// ███████   ████    ██████  ███████    ██     ██████  ██   ██ ███████
		// 
		// https://www.npmjs.com/package/grunt-svgstore
		// */
		// #svgstore
		
		svgstore: {
			options: {
				prefix: name.css(['svg', 'sprite']) + '-',
				svg: {
					id: name.css(['svg', 'sprite']),
					xmlns: 'http://www.w3.org/2000/svg',
					style: 'display:none',
				},
				cleanup: false,
				includeTitleElement: false,
			},
			build: {
				files: {
					'demo/assets/svg-sprite.svg': ['runtime/svg-sprite/*'],
				},
			},
		},

		// /*
		//  ██████ ██      ███████  █████  ███    ██
		// ██      ██      ██      ██   ██ ████   ██
		// ██      ██      █████   ███████ ██ ██  ██
		// ██      ██      ██      ██   ██ ██  ██ ██
		//  ██████ ███████ ███████ ██   ██ ██   ████
		// */
		// #clean
		
		clean: {
			demo: 'demo/assets/*',
			js: 'demo/assets/*.js',
			css: 'demo/assets/*.css',
			svg: 'demo/assets/svg-sprite.svg',
			dist: 'dist/*',
			svg_runtime: 'runtime/svg-sprite/*',
		},

		// /*
		//  ██████  ██████  ███    ██  ██████ ██    ██ ██████  ██████  ███████ ███    ██ ████████
		// ██      ██    ██ ████   ██ ██      ██    ██ ██   ██ ██   ██ ██      ████   ██    ██
		// ██      ██    ██ ██ ██  ██ ██      ██    ██ ██████  ██████  █████   ██ ██  ██    ██
		// ██      ██    ██ ██  ██ ██ ██      ██    ██ ██   ██ ██   ██ ██      ██  ██ ██    ██
		//  ██████  ██████  ██   ████  ██████  ██████  ██   ██ ██   ██ ███████ ██   ████    ██
		// */
		// #concurrent

		concurrent: {
			options: {
				logConcurrentOutput: true,
			},
			default: [
				'watch:css',
				'watch:img',
				'watch:svg',
			],
		},

		// /*
		// ██     ██  █████  ████████  ██████ ██   ██
		// ██     ██ ██   ██    ██    ██      ██   ██
		// ██  █  ██ ███████    ██    ██      ███████
		// ██ ███ ██ ██   ██    ██    ██      ██   ██
		//  ███ ███  ██   ██    ██     ██████ ██   ██
		// */
		// #watch
		watch: {
			options: {
				spawn: false,
			},
			css: {
				files: [
					'src/css/**/*.scss',
				],
				tasks: [
					'sass:demo',
				],
			},
			svg: {
				files: ['src/img/svg-sprite/*.svg'],
				tasks: [
					'clean:svg_runtime',
					'imagemin:svg',
					'svgstore:build',
				],
			},
		},
		
	});
	
	grunt.registerTask('default', 'concurrent');
	grunt.registerTask('build', 'build:dist');
	grunt.registerTask('build:dist', [
		'sass:dist',
	]);
	grunt.registerTask('build:demo', [
		'sass:demo',
		'svg',
	]);
	grunt.registerTask('svg', [
		'clean:svg_runtime',
		'imagemin:svg',
		'svgstore:build',
	]);
};