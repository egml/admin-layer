<?php
	require __DIR__ . '/../vendor/autoload.php';
 
	$templateEngine = new Pug;
	$template = file_get_contents(__DIR__ . '/../src/html/access-point.pug');
	$data = [
		'parentFontSize' => 14,
		'url' => 'urls/login.php',
	];
	$accessPointHtml = $templateEngine->render($template, $data);
 
	$path = 'assets/';
	if (isset($_GET['dist'])) {
		$path = '../dist/';
	}
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Гость - Демонстрация Admin Layer</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/demo.css">
	<link rel="stylesheet" href="<?= $path ?>egml-admin-layer-guest.css">
	
	<script src="<?= $path ?>egml-admin-layer-guest.js"></script>
<body>
	<script>
		egml.adminLayer.config.rootFontSize = 14;
		egml.adminLayer.config.publicPath = '<?= $path ?>';
		egml.adminLayer.config.innerStyleUrl = '../assets/inner-style.css';
	</script>

	<div style="margin:3rem 5rem;">
		<p>
			Кристаллическая решетка, как и везде в пределах наблюдаемой вселенной, выталкивает магнит. Волновая тень принципиально <?= $accessPointHtml ?> неизмерима. Исследователями из разных лабораторий неоднократно наблюдалось, как колебание ненаблюдаемо. Еще в ранних работах Л.Д.Ландау показано, что плазменное образование отталкивает наносекундный кварк при любом их взаимном расположении. Силовое поле, если рассматривать процессы в рамках специальной теории относительности, когерентно сжимает межядерный экситон, даже если пока мы не можем наблюсти это непосредственно.
		</p>
 
		<?= $accessPointHtml ?>
	</div>
</body>
</html>