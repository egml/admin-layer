<?php
	require __DIR__ . '/../vendor/autoload.php';

	$rootFontSize = 14;

	$templateEngine = new Pug;
	$template = file_get_contents(__DIR__ . '/../src/html/panel.pug');
	$data = [
		'url' => '#',
		'parentFontSize' => $rootFontSize,
	];
	$html = $templateEngine->render($template, $data);
	function render($newData) {
		global $templateEngine, $template, $data;
		return $templateEngine->render($template, array_merge($data, $newData));
	}
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Панель - Демонстрация Admin Layer</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/demo.css">
	<link rel="stylesheet" href="assets/panel.css">
</head>
<body>

	<div style="margin:3rem 5rem;">

		<style>
			.left-col {
				width: 18rem;
			}
			.right-col {
				font-size: 4rem;
			}
		</style>

		<?php /* ?>
		<div class="row row--margin-40">
			<div class="left-col">
				Статичное состояние: 
			</div>
			<div class="right-col">
				<?= $html ?>
			</div>
		</div>
		<?php */ ?>

		<p>
			Кристаллическая решетка, как и везде в пределах наблюдаемой вселенной, выталкивает магнит. Волновая тень принципиально неизмерима. Исследователями из разных лабораторий неоднократно наблюдалось, как колебание ненаблюдаемо. Еще в ранних работах Л.Д.Ландау показано, что плазменное образование отталкивает наносекундный кварк при любом их взаимном расположении. Силовое поле, если рассматривать процессы в рамках специальной теории относительности, когерентно сжимает межядерный экситон, даже если пока мы не можем наблюсти это непосредственно.
		</p>


	</div>

	<?= $html ?>

	<?php include 'assets/svg-sprite.svg' ?>

</body>
</html>