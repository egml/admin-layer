<?php
	require __DIR__ . '/../vendor/autoload.php';

	$templateEngine = new Pug;
	$template = file_get_contents(__DIR__ . '/../src/html/access-point.pug');
	$data = [
		'url' => '#',
		'parentFontSize' => 14,
	];
	$html = $templateEngine->render($template, $data);
	function render($newData) {
		global $templateEngine, $template, $data;
		return $templateEngine->render($template, array_merge($data, $newData));
	}
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Точка входа - Демонстрация Admin Layer</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/demo.css">
	<link rel="stylesheet" href="assets/access-point.css">
</head>
<body>

	<div style="margin:3rem 5rem;">

		<style>
			.left-col {
				width: 18rem;
			}
			.right-col {
				font-size: 4rem;
			}
		</style>

		<div class="row row--margin-40">
			<div class="left-col">
				Статичное состояние: 
			</div>
			<div class="right-col">
				<?= $html ?>
			</div>
		</div>

		<div class="row row--margin-40">
			<div class="left-col">
				При наведении:
			</div>
			<div class="right-col">
				<?= render([ 'classes' => ' egml-admin_layer-access_point--hover' ]) ?>
			</div>
		</div>

		<div class="row row--margin-40">
			<div class="left-col">
				При фокусе:
			</div>
			<div class="right-col">
				<?= render([ 'classes' => ' egml-admin_layer-access_point--focus' ]) ?>
			</div>
		</div>
		
		<div class="row row--margin-40">
			<div class="left-col">
				"Идет загрузка":
			</div>
			<div class="right-col">
				<?= $html ?>
				
				<span style="font-size:1.4rem;">
					<script>
						function classToggle() {
							this.parentNode.parentNode.querySelector('a').classList.toggle('egml-admin_layer-access_point--loading');
							this.textContent = this.textContent == 'Включить' ? 'Выключить' : 'Включить';
						}
					</script>
					<button onclick="classToggle.call(this)">Включить</button>
				</span>
			</div>
		</div>

		<p>
			Кристаллическая решетка, как и везде в пределах наблюдаемой вселенной, выталкивает магнит. Волновая тень принципиально <?= $html ?> неизмерима. Исследователями из разных лабораторий неоднократно наблюдалось, как колебание ненаблюдаемо. Еще в ранних работах Л.Д.Ландау показано, что плазменное образование отталкивает наносекундный кварк при любом их взаимном расположении. Силовое поле, если рассматривать процессы в рамках специальной теории относительности, когерентно сжимает межядерный экситон, даже если пока мы не можем наблюсти это непосредственно.
		</p>

		<?= $html ?>

	</div>

	<?php include 'assets/svg-sprite.svg' ?>

</body>
</html>