<?php
	require __DIR__ . '/../vendor/autoload.php';

	$rootFontSize = 14;

	$templateEngine = new Pug;
	$template = file_get_contents(__DIR__ . '/../src/html/modal.pug');
	$data = [
		'parentFontSize' => $rootFontSize,
	];
	$html = $templateEngine->render($template, $data);
	function render($newData) {
		global $templateEngine, $template, $data;
		return $templateEngine->render($template, array_merge($data, $newData));
	}
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Модальное окно - Демонстрация Admin Layer</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="assets/demo.css">
	<link rel="stylesheet" type="text/css" media="screen" href="assets/modal.css">
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			var modal = document.querySelector('.egml-admin_layer-modal');
			if (modal) {
				modal.style.display = null;
			}
		});
	</script>
</head>
<body>
	<div style="margin:0 auto; width:70rem;">
		<p>
			Кристаллическая решетка, как и везде в пределах наблюдаемой вселенной, выталкивает магнит. Волновая тень принципиально неизмерима. Исследователями из разных лабораторий неоднократно наблюдалось, как колебание ненаблюдаемо. Еще в ранних работах Л.Д.Ландау показано, что плазменное образование отталкивает наносекундный кварк при любом их взаимном расположении. Силовое поле, если рассматривать процессы в рамках специальной теории относительности, когерентно сжимает межядерный экситон, даже если пока мы не можем наблюсти это непосредственно.
		</p>

	</div>

	<?= $html ?>
	
	<?php /* ?>
	<?php */ ?>
	<?php /* ?>
	<div class="egml-admin_layer-modal" style="font-size:<?= 16 / $parentFontSize ?>em">
		<div class="egml-admin_layer-modal-overlay"></div>
		<div class="egml-admin_layer-modal-clip">
			<div class="egml-admin_layer-modal-box">
				<div class="egml-admin_layer-modal-content">
					<iframe class="egml-admin_layer-modal-content-iframe" src="dashboard.html" scrolling="no" aria-hidden="true"></iframe>
				</div>
				<button class="egml-admin_layer-modal-close_trigger" title="Закрыть">
					<svg class="egml-admin_layer-modal-close_trigger-icon" role="presentation" aria-hidden="true">
						<title>Закрыть диалог</title>
						<use xlink:href="#egml-admin_layer-svg_sprite-cross"></use>
					</svg>
				</button>
			</div>
		</div>
	</div>
	<?php /**/ ?>

	<?php include 'assets/svg-sprite.svg' ?>

</body>
</html>