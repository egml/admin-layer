(function (config) {
	'use strict';

	config = config && config.hasOwnProperty('default') ? config['default'] : config;

	function template(data) {
		var template = 
`<div class="egml-admin_layer-modal" style="display:none; font-size:${ 16 / data.parentFontSize }em">
	<div class="egml-admin_layer-modal-clip">
		<div class="egml-admin_layer-modal-aligner">
			<div class="egml-admin_layer-modal-aligner-helper">
				<div class="egml-admin_layer-modal-box">
					<div class="egml-admin_layer-modal-content">
						<iframe class="egml-admin_layer-modal-content-iframe" src="" scrolling="no" aria-hidden="true"></iframe>
					</div>
					<button class="egml-admin_layer-modal-close" title="Закрыть">
						<svg class="egml-admin_layer-modal-close-icon" role="presentation" aria-hidden="true">
							<title>Закрыть диалог</title>
							<use xlink:href="#egml-admin_layer-svg_sprite-cross"></use>
						</svg>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>`	;
		return template;
	}

	var data = {
		parentFontSize: config.rootFontSize,
	};
	var domFragment = document.createRange().createContextualFragment(template(data));
	// #CBNOTE: Старые браузеры, такие как WebView 33, не поддерживают createContextualFragment (или что-то в этом духе), в общем, если ругаются, то вот так можно обойти:
	// var parser = new DOMParser();
	// var modalDom = parser.parseFromString(html(data), 'text/html');
	// var domFragment = document.importNode(modalDom.documentElement, true);

	// Встраивание HTML модального окна в DOM
	document.body.appendChild(domFragment);

}(egml.adminLayer.config));
