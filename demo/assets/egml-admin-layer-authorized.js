this.egml = this.egml || {};
this.egml.adminLayer = (function (exports) {
	'use strict';

	//  ██████  ███████  ██████  ██    ██ ███████ ███████ ████████

	//  ███████ ██   ██ ████████ ███████ ███    ██ ██████
	//  ██       ██ ██     ██    ██      ████   ██ ██   ██
	//  █████     ███      ██    █████   ██ ██  ██ ██   ██
	//  ██       ██ ██     ██    ██      ██  ██ ██ ██   ██
	//  ███████ ██   ██    ██    ███████ ██   ████ ██████
	//
	// #extend
	function extend(dest, source) {
		for (var prop in source) {
			dest[prop] = source[prop];
		}
		return dest;
	}

	//  ██████   █████   ██████  ███████
	//  ██   ██ ██   ██ ██       ██
	//  ██████  ███████ ██   ███ █████
	//  ██      ██   ██ ██    ██ ██
	//  ██      ██   ██  ██████  ███████
	// 
	//  ███████  ██████ ██████   ██████  ██      ██
	//  ██      ██      ██   ██ ██    ██ ██      ██
	//  ███████ ██      ██████  ██    ██ ██      ██
	//       ██ ██      ██   ██ ██    ██ ██      ██
	//  ███████  ██████ ██   ██  ██████  ███████ ███████
	//
	// #page #scroll
	// Кросс-браузерное определение величины скролла
	function pageScroll() {
		if (window.pageXOffset != undefined) {
			return {
				left: pageXOffset,
				top: pageYOffset
			};
		} else {
			var html = document.documentElement;
			var body = document.body;

			var top = html.scrollTop || body && body.scrollTop || 0;
			top -= html.clientTop;

			var left = html.scrollLeft || body && body.scrollLeft || 0;
			left -= html.clientLeft;

			return {
				left: left,
				top: top
			};
		}
	}

	//  ███████ ████████ ██████  ██ ███    ██  ██████
	//  ██         ██    ██   ██ ██ ████   ██ ██
	//  ███████    ██    ██████  ██ ██ ██  ██ ██   ███
	//       ██    ██    ██   ██ ██ ██  ██ ██ ██    ██
	//  ███████    ██    ██   ██ ██ ██   ████  ██████
	// 
	//   ██████  █████  ███████ ███████
	//  ██      ██   ██ ██      ██
	//  ██      ███████ ███████ █████
	//  ██      ██   ██      ██ ██
	//   ██████ ██   ██ ███████ ███████
	//
	// #string #case
	function stringCase(bits, type) {
		if (!bits || !bits.length) {
			return '';
		}
		if (typeof bits == 'string') {
			bits = [ bits ];
		}
		var string =  '';
		switch (type) {
			case 'camel':
				bits.forEach(function(bit, i) {
					if (i == 0) {
						string += firstLowerCase(bit);
					} else {
						string += firstUpperCase(bit);
					}
				});
				break;

			case 'capitalCamel':
				bits.forEach(function(bit) {
					string += firstUpperCase(bit);
				});
				break;

			case 'flat':
				string = bits.join('');
				break;

			case 'snake':
				string = bits.join('_');
				break;

			case 'kebab':
				string = bits.join('-');
				break;

			case 'dot':
				string = bits.join('.');
				break;
		
			case 'asIs':
			default:
				string = bits.join('');
				break;
		}
		return string;
	}

	//  ███████ ██ ██████  ███████ ████████
	//  ██      ██ ██   ██ ██         ██
	//  █████   ██ ██████  ███████    ██
	//  ██      ██ ██   ██      ██    ██
	//  ██      ██ ██   ██ ███████    ██
	// 
	//  ██    ██ ██████  ██████  ███████ ██████      ██
	//  ██    ██ ██   ██ ██   ██ ██      ██   ██    ██
	//  ██    ██ ██████  ██████  █████   ██████    ██
	//  ██    ██ ██      ██      ██      ██   ██  ██
	//   ██████  ██      ██      ███████ ██   ██ ██
	// 
	//  ██       ██████  ██     ██ ███████ ██████
	//  ██      ██    ██ ██     ██ ██      ██   ██
	//  ██      ██    ██ ██  █  ██ █████   ██████
	//  ██      ██    ██ ██ ███ ██ ██      ██   ██
	//  ███████  ██████   ███ ███  ███████ ██   ██
	// 
	//   ██████  █████  ███████ ███████
	//  ██      ██   ██ ██      ██
	//  ██      ███████ ███████ █████
	//  ██      ██   ██      ██ ██
	//   ██████ ██   ██ ███████ ███████
	//
	// #first #upper #lower #case
	function firstUpperCase(string) {
		return string[0].toUpperCase() + string.slice(1);
	}function firstLowerCase(string) {
		return string[0].toLowerCase() + string.slice(1);
	}
	// #TODO: Написать функционал логинга переменной или объекта в HTML
	// //  ██████   ██████  ███    ███
	// //  ██   ██ ██    ██ ████  ████
	// //  ██   ██ ██    ██ ██ ████ ██
	// //  ██   ██ ██    ██ ██  ██  ██
	// //  ██████   ██████  ██      ██
	// // 
	// //  ██       ██████   ██████
	// //  ██      ██    ██ ██
	// //  ██      ██    ██ ██   ███
	// //  ██      ██    ██ ██    ██
	// //  ███████  ██████   ██████
	// //
	// // #dom #log
	// export function domLog(object, target) {
	// 	if (typeof target == 'undefined') {
	// 		target = document;
	// 	}
	// 	var output = '';
	// 	if (typeof object == 'object') {
	// 		for (var key in object) {
	// 			if (object.hasOwnProperty(key)) {
	// 				var element = object[key];
	// 				output += '';
	// 			}
	// 		}
	// 	}
	// 	'<div style="font-family:monospace; font-size:1rem; white-space:nowrap;">' + 
	// 	'width &nbsp;= ' + rect.width + '<br>' +
	// 	'height = ' + rect.height + '<br>' +
	// 	'x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= ' + rect.x + '<br>' +
	// 	'y &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= ' + rect.y + '<br>' +
	// 	'left &nbsp;&nbsp;= ' + rect.left + '<br>' +
	// 	'top &nbsp;&nbsp;&nbsp;= ' + rect.top + '<br>' +
	// 	'right &nbsp;= ' + rect.right + '<br>' +
	// 	'bottom = ' + rect.bottom + '<br>' +
	// 	'</div>';
	// }

	//	██ ███    ██      ██ ███████  ██████ ████████
	//	██ ████   ██      ██ ██      ██         ██
	//	██ ██ ██  ██      ██ █████   ██         ██
	//	██ ██  ██ ██ ██   ██ ██      ██         ██
	//	██ ██   ████  █████  ███████  ██████    ██
	// 
	//	███████ ████████ ██    ██ ██      ███████
	//	██         ██     ██  ██  ██      ██
	//	███████    ██      ████   ██      █████
	//	     ██    ██       ██    ██      ██
	//	███████    ██       ██    ███████ ███████
	// 
	// #inject #style
	function injectStyle() {
		var options = {
			url: null,
			content: null,
			inline: null,
			mountElement: null,
			targetDocument: null,
			onLoadCallback: null,
		};
		
		var mountElement = null;
		
		options.inline = false;
		options.targetDocument = document;
		
		Object.defineProperty(options, 'mountElement', {
			get: function() {
				if (mountElement == null) {
					return options.targetDocument.head;
				} else {
					return mountElement;
				}
			},
			set: function(value) {
				mountElement = value;
			},
			configurable: true,
			enumerable: true,
		});
		
		if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
			options.url = arguments[0];
			options.onLoadCallback = arguments[1];
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			extend(options, arguments[0]);
		} else {
			return;
		}
		
		if (options.url) {
			if (options.inline == false) {
				var tag = options.targetDocument.createElement('link');
				tag.rel = 'stylesheet';
				if (typeof options.onLoadCallback == 'function') {
					tag.addEventListener('load', options.onLoadCallback);
				}
				tag.href = options.url;
				options.targetDocument.head.appendChild(tag);
			} else {
				var xhr = new XMLHttpRequest();
				xhr.open('get', options.url);
				xhr.onload = function() {
					var tag = options.targetDocument.createElement('style');
					tag.textContent = this.responseText;
					options.mountElement.appendChild(tag);
					if (typeof options.onLoadCallback == 'function') {
						options.onLoadCallback();
					}
				};
				xhr.send();
			}
		} else if (options.content) {
			var tag = options.targetDocument.createElement('style');
			tag.textContent = options.content;
			options.mountElement.appendChild(tag);
			if (typeof options.onLoadCallback == 'function') {
				options.onLoadCallback();
			}
		} else {
			return;
		}
	}

	//	██ ███    ██      ██ ███████  ██████ ████████
	//	██ ████   ██      ██ ██      ██         ██
	//	██ ██ ██  ██      ██ █████   ██         ██
	//	██ ██  ██ ██ ██   ██ ██      ██         ██
	//	██ ██   ████  █████  ███████  ██████    ██
	// 
	//	██   ██ ████████ ███    ███ ██
	//	██   ██    ██    ████  ████ ██
	//	███████    ██    ██ ████ ██ ██
	//	██   ██    ██    ██  ██  ██ ██
	//	██   ██    ██    ██      ██ ███████
	// 
	//	#inject
	// export function injectHtml(url, inline, targetDocument, legacy) {}

	//	██ ███    ██      ██ ███████  ██████ ████████
	//	██ ████   ██      ██ ██      ██         ██
	//	██ ██ ██  ██      ██ █████   ██         ██
	//	██ ██  ██ ██ ██   ██ ██      ██         ██
	//	██ ██   ████  █████  ███████  ██████    ██
	// 
	//	███████ ██    ██  ██████
	//	██      ██    ██ ██
	//	███████ ██    ██ ██   ███
	//	     ██  ██  ██  ██    ██
	//	███████   ████    ██████
	// 
	//	███████ ██    ██ ███    ███ ██████   ██████  ██
	//	██       ██  ██  ████  ████ ██   ██ ██    ██ ██
	//	███████   ████   ██ ████ ██ ██████  ██    ██ ██
	//	     ██    ██    ██  ██  ██ ██   ██ ██    ██ ██
	//	███████    ██    ██      ██ ██████   ██████  ███████
	// 
	//	#inject #svg #symbol
	function injectSvgSymbol() {
		// -------------------------------------------
		//  Вариант с отдельным родительским
		// 	SVG-элементом для каждого символа:
		// -------------------------------------------
		var options = {
			symbol: null,
			mountElement: document.body,
			containerClass: null,
		};
		
		if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
			options.symbol = arguments[0];
			options.mountElement = arguments[1] || options.mountElement;
			options.containerClass = arguments[2] || options.containerClass;
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			extend(options, arguments[0]);
		} else {
			return;
		}

		var parser = new DOMParser();
		var symbolDocument = parser.parseFromString(
			`<svg xmlns="http://www.w3.org/2000/svg" 
			${ options.containerClass ? `class="${options.containerClass}"` : '' } 
			style="display: none;">
			${options.symbol}
		</svg>`,
			'image/svg+xml'
		);
		var symbolNode = document.importNode(symbolDocument.documentElement, true);
		options.mountElement.appendChild(symbolNode);

		// -------------------------------------------
		//  Вариант со вставкой в один родительский 
		// 	SVG-элемент:
		// -------------------------------------------
		// var options = {
		// 	symbol: null,
		// 	containerId: 'svg_sprite',
		// 	mountElement: document.body,
		// };
		 
		// if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
		// 	options.symbol = arguments[0];
		// 	options.containerId = arguments[1] || options.containerId;
		// 	options.mountElement = arguments[2] || options.mountElement;
		// } else if (!!arguments[0] && typeof arguments[0] == 'object') {
		// 	extend(options, arguments[0]);
		// } else {
		// 	return;
		// }

		// var container = document.getElementById(options.containerId);
		
		// if (container == null) {
		// 	container = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		// 	container.setAttribute('id', options.containerId);
		// 	container.style.cssText = 'display: none;';
		// 	options.mountElement.appendChild(container);
		// }

		// var parser = new DOMParser();
		// var symbolDocument = parser.parseFromString(
		// 	`<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
		// 		${options.symbol}
		// 	</svg>`,
		// 	'image/svg+xml'
		// );
		// var symbolNode = document.importNode(symbolDocument.querySelector('symbol'), true);
		// container.appendChild(symbolNode);
	}

	function StringCase(bits) {
		this.bits = bits;

		if (Array.isArray(bits)) {
			this.asIs = this.bits.join('');
			bits.forEach(function(bit, i) {
				bits[i] = bit.toLowerCase();
			});
		} else {
			this.asIs = this.bits;
			bits = bits.toLowerCase();
		}

		this.camel = stringCase(bits, 'camel');
		this.capitalCamel = stringCase(bits, 'capitalCamel');
		this.kebab = stringCase(bits, 'kebab');
		this.snake = stringCase(bits, 'snake');
		this.flat = stringCase(bits, 'flat');
		this.dot = stringCase(bits, 'dot');
	}
	StringCase.prototype.toString = function() {
		return this.asIs;
	};

	function Name(nameBits, nsBits) {
		var bits;

		// Далее идет обработка параметра nsBits:
		// - если он не задан, пропустить все и взять ns из прототипа
		// - если он boolean и true, то также пропустить и использовать ns 
		//   из прототипа
		// - если он boolean и false, то nsBits - пустая строка
		// - и если в результате nsBits строка или массив, то создать новое 
		//   свойство ns поверх геттера прототипа со значением nsBits
		if (nsBits != null) {
			if (typeof nsBits == 'boolean' && nsBits == false) {
				nsBits = '';
			}
			if (typeof nsBits == 'string' || Array.isArray(nsBits)) {
				Object.defineProperty(this, 'ns', {
					value: new StringCase(nsBits),
					writable: true,
					configurable: true,
					enumerable: true,
				});
			}
		}

		var hasNs = this.ns && this.ns.toString() != '';

		this.base = new StringCase(nameBits);

		// #asIs #flat #camel #capital #kebab
		var types = ['asIs', 'flat', 'camel', 'capitalCamel', 'kebab'];
		for (var i = 0; i < types.length; i++) {
			if (hasNs) {
				this['_' + types[i]] = stringCase([this.ns[types[i]], this.base[types[i]]], types[i]);
			} else {
				this['_' + types[i]] = this.base[types[i]];
			}
			this[types[i]] = (function(type, nameObj) {
				return function(suffix) {
					if (suffix) {
						if (type != 'asIs') {
							if (Array.isArray(suffix)) {
								suffix.forEach(function(bit, i) {
									suffix[i] = bit.toLowerCase();
								});
							}
						}
						return stringCase([nameObj['_' + type], stringCase(suffix, type)], type);
					} else {
						return nameObj['_' + type];
					}
				};
			})(types[i], this);
		}

		//  ██████   ██████  ████████
		//  ██   ██ ██    ██    ██
		//  ██   ██ ██    ██    ██
		//  ██   ██ ██    ██    ██
		//  ██████   ██████     ██
		//
		// #dot
		var bits = [ this.base.camel ];
		if (hasNs) {
			bits.unshift(this.ns.camel);
		}
		this._dot = stringCase(bits, 'dot');
		this.dot = function(suffix) {
			if (suffix) {
				if (Array.isArray(suffix)) {
					suffix.forEach(function(bit, i) {
						suffix[i] = bit.toLowerCase();
					});
				}
				return stringCase([this._dot, stringCase(suffix, 'camel')], 'dot');
			} else {
				return this._dot;
			}
		};

		//  ███████ ██    ██ ███████ ███    ██ ████████
		//  ██      ██    ██ ██      ████   ██    ██
		//  █████   ██    ██ █████   ██ ██  ██    ██
		//  ██       ██  ██  ██      ██  ██ ██    ██
		//  ███████   ████   ███████ ██   ████    ██
		//
		// #event
		this.event = this.dot;

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		//
		// #css
		bits = [ this.base.snake ];
		if (hasNs) {
			bits.unshift(this.ns.snake);
		}
		this._css = stringCase(bits, 'kebab');
		this.css = function(suffix) {
			if (suffix) {
				if (Array.isArray(suffix)) {
					suffix.forEach(function(bit, i) {
						suffix[i] = bit.toLowerCase();
					});
				}
				return stringCase([this._css, stringCase(suffix, 'snake')], 'kebab');
			} else {
				return this._css;
			}
		};

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		// 
		//  ███    ███  ██████  ██████
		//  ████  ████ ██    ██ ██   ██
		//  ██ ████ ██ ██    ██ ██   ██
		//  ██  ██  ██ ██    ██ ██   ██
		//  ██      ██  ██████  ██████
		//
		// #css #modificator
		this.cssModificator = function(modificatorName) {
			if (modificatorName != null) {
				if (Array.isArray(modificatorName)) {
					modificatorName.forEach(function(bit, i) {
						modificatorName[i] = bit.toLowerCase();
					});
				}
				return this.css() + '--' + stringCase(modificatorName, 'snake');
			} else {
				return this.css();
			}
		};
		this.cssMod = this.cssModificator;

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		// 
		//   ██████ ██       █████  ███████ ███████
		//  ██      ██      ██   ██ ██      ██
		//  ██      ██      ███████ ███████ ███████
		//  ██      ██      ██   ██      ██      ██
		//   ██████ ███████ ██   ██ ███████ ███████
		// 
		//  ███████ ███████ ██      ███████  ██████ ████████  ██████  ██████
		//  ██      ██      ██      ██      ██         ██    ██    ██ ██   ██
		//  ███████ █████   ██      █████   ██         ██    ██    ██ ██████
		//       ██ ██      ██      ██      ██         ██    ██    ██ ██   ██
		//  ███████ ███████ ███████ ███████  ██████    ██     ██████  ██   ██
		//
		// #css #class #selector
		this.cssClassSelector = function(appendage) {
			return '.' + this.css(appendage);
		};
		this.selector = function(appendage) {
			return this.cssClassSelector(appendage);
		};
	}
	var ns;
	Object.defineProperty(Name.prototype, 'ns', {
		set: function(value) {
			ns = new StringCase(value);
		},
		get: function() {
			return ns;
		},
	});
	Name.prototype.toString = function() {
		return this._asIs;
	};

	var pkg = {name:"@egml/admin-layer",version:"1.0.0","private":true,egml:{name:["admin","layer"],namespace:"egml"},sideEffects:["*.scss","*.svg"],scripts:{js:"rollup --config","js-watch":"rollup --config --watch","js-webpack":"webpack --config-name demo","js-webpack-watch":"webpack --config-name demo --watch","js-build":"rollup --config build.rollup.config.js",css:"grunt sass:demo","css-watch":"grunt watch:css","css-build":"grunt sass:dist",clean:"grunt clean:demo","clean-js":"grunt clean:js","clean-css":"grunt clean:css","clean-svg":"grunt clean:svg","clean-build":"grunt clean:dist",svg:"grunt svg",build:"yarn clean-build && grunt build && yarn js-build"},dependencies:{"@egml/utils":"https://bitbucket.org/egml/utils",jquery:"^3.3.1","jquery.transit":"https://github.com/egml/jquery.transit","spin.js":"^2.3.2"},devDependencies:{"@babel/core":"^7.3.4","@babel/plugin-syntax-dynamic-import":"^7.2.0","@babel/polyfill":"^7.2.5","@babel/preset-env":"^7.3.4","babel-loader":"^8.0.5","cookies.js":"git+https://github.com/madmurphy/cookies.js.git","css-loader":"^2.1.0",grunt:"^1.0.3","grunt-concurrent":"^2.3.1","grunt-contrib-clean":"^2.0.0","grunt-contrib-copy":"^1.0.0","grunt-contrib-imagemin":"^3.1.0","grunt-contrib-watch":"^1.1.0","grunt-newer":"^1.3.0","grunt-notify":"^0.4.5","grunt-sass":"^3.0.2","grunt-svgstore":"^2.0.0","handlebars-loader":"^1.7.1","html-webpack-plugin":"^4.0.0-beta.5","load-grunt-tasks":"^4.0.0","module-alias":"^2.2.0","mustache-loader":"^1.4.3","node-sass":"^4.11.0","node-sass-import":"^2.0.1","normalize-scss":"^7.0.1","normalize.css":"^8.0.1","pug-loader":"^2.4.0",rollup:"^1.2.2","rollup-plugin-butternut":"^0.1.0","rollup-plugin-commonjs":"^9.2.1","rollup-plugin-json":"^3.1.0","rollup-plugin-mustache":"^0.1.0","rollup-plugin-node-resolve":"^4.0.1","rollup-plugin-progress":"^1.0.0","rollup-plugin-pug":"^1.1.0","rollup-plugin-sass":"^1.1.0","rollup-plugin-sizes":"^0.5.1","rollup-plugin-string":"^3.0.0","rollup-plugin-svg-to-symbol":"^1.0.0","rollup-plugin-svgo":"^1.0.2",sass:"^1.16.0","sass-loader":"^7.1.0","style-loader":"^0.23.1","svg-inline-loader":"^0.8.0","svg-sprite-loader":"^4.1.3","svgo-loader":"^2.2.0","url-loader":"^1.1.2",webpack:"^4.28.4","webpack-bundle-analyzer":"^3.1.0","webpack-cli":"^3.2.1","webpack-notifier":"^1.7.0"}};

	//	 ██████  ██████  ███    ██ ███████ ██  ██████

	Name.prototype.ns = pkg.egml.namespace;
	 
	var config = {
		name: new Name(pkg.egml.name),
		publicPath: null,
		rootFontSize: 16,
		innerStyleUrl: null,
	};
	  
	Object.defineProperty(config, 'global', {
		get: function() {
			return window[config.name.ns.camel][config.name.base.camel];
		}
	});

	var iconCog = "<symbol id=\"egml-admin_layer-svg_sprite-cog_stroke\" viewBox=\"0 0 100 100\"><path style=\"line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000;text-transform:none;block-progression:tb;white-space:normal;isolation:auto;mix-blend-mode:normal;solid-color:#000;solid-opacity:1\" d=\"M 37.273438,0 36.441406,3.5664062 33.564453,15.892578 c -1.54098,0.749627 -3.019658,1.596166 -4.439453,2.542969 L 13.554688,13.443359 0.38671875,36.746094 12.28125,47.175781 c -0.08188,0.938116 -0.220703,1.833178 -0.220703,2.822266 0,0.852635 0.11375,1.617693 0.175781,2.427734 L 0.22265625,62.955078 13.390625,86.261719 28.771484,81.332031 c 1.46408,0.998044 2.991015,1.885437 4.582032,2.669922 l 2.789062,15.335938 26.75,0.662109 3.72461,-15.970703 c 1.391862,-0.687563 2.732611,-1.449674 4.021484,-2.296875 L 85.148438,87.179688 99.441406,64.544922 87.677734,53.171875 c 0.102468,-1.050388 0.261719,-2.059463 0.261719,-3.173828 0,-0.763255 -0.09824,-1.446695 -0.148437,-2.173828 L 99.777344,37.320312 86.603516,14.017578 71.511719,18.855469 c -1.495823,-1.03917 -3.057302,-1.963266 -4.6875,-2.777344 L 64.023438,0.66210938 37.273438,0 Z m 7.099609,9.1777344 12.105469,0.3007812 2.330078,12.8203124 2.265625,0.939453 c 2.457912,1.018966 4.780734,2.401524 6.914062,4.097657 l 1.884766,1.496093 12.501953,-4.007812 5.964844,10.552734 -9.941406,8.712891 0.291015,2.365234 c 0.153094,1.241055 0.25,2.416508 0.25,3.542969 0,1.362969 -0.139368,2.785286 -0.361328,4.273437 l -0.341797,2.289063 9.6875,9.367187 -6.470703,10.25 -12.105469,-4.544922 -1.974609,1.486329 c -2.003266,1.507649 -4.134849,2.746972 -6.369141,3.666015 l -2.144531,0.88086 -3.066406,13.15625 L 43.6875,90.521484 41.365234,77.748047 39.078125,76.814453 c -2.425985,-0.989475 -4.72166,-2.331764 -6.839844,-3.982422 l -1.876953,-1.460937 -12.738281,4.083984 -5.964844,-10.554687 9.994141,-8.759766 -0.3125,-2.384766 c -0.17203,-1.314186 -0.279297,-2.56359 -0.279297,-3.757812 0,-1.309268 0.126486,-2.667492 0.330078,-4.091797 l 0.34375,-2.412109 -9.912109,-8.691407 5.96289,-10.550781 12.873047,4.126953 1.865235,-1.417968 c 2.070313,-1.573114 4.291693,-2.856839 6.628906,-3.802735 L 41.314453,22.28125 44.373047,9.1777344 Z M 50,31.158203 C 39.651378,31.15901 31.170726,39.652401 31.169922,50.001953 31.169306,60.352559 39.651236,68.844756 50,68.845703 60.349787,68.846176 68.832719,60.354149 68.832031,50.001953 68.831228,39.650772 60.349624,31.157518 50,31.158203 Z m 0,9 c 5.480182,-3.63e-4 9.831605,4.352963 9.832031,9.84375 3.65e-4,5.491333 -4.350384,9.844001 -9.832031,9.84375 -5.481074,-5.01e-4 -9.830405,-4.352278 -9.830078,-9.84375 C 40.170348,44.511169 44.52034,40.15863 50,40.158203 Z\" color=\"#000\" font-weight=\"400\" font-family=\"sans-serif\" overflow=\"visible\" stroke-width=\"9\"/></symbol>";

	var iconToggle = "<symbol id=\"egml-admin_layer-svg_sprite-editable_area_toggle\" viewBox=\"0 0 20 20\"><path d=\"M 1.411,19.999 0,18.588 l 2.044,-2.044 0,-14.549 14.55,0 L 18.59,0 20,1.411 Z M 4.04,3.99 4.04,14.549 14.6,3.99 Z m 7.981,13.964 -3.992,0 0,-1.994 3.991,0 0,1.994 z m 3.989,-9.974 1.995,0 0,3.99 -1.995,0 z m 0,5.985 1.995,0 0,3.989 -3.989,0 0,-1.994 1.994,0 z\" clip-rule=\"evenodd\" fill-rule=\"evenodd\"/></symbol>";

	var iconCross = "<symbol id=\"egml-admin_layer-svg_sprite-cross_shape\" viewBox=\"0 0 20 20\"><path d=\"M 16.589922,2.0000143 10,8.5890049 3.4120547,2.0019908 1.9999453,3.4120092 8.5898438,10.000976 1.9999219,16.587991 3.4100781,17.998009 10,11.410995 l 6.589922,6.589014 1.410156,-1.410018 -6.589922,-6.589015 6.589922,-6.5889903 z\" clip-rule=\"evenodd\" fill-rule=\"evenodd\"/></symbol>";

	var dom = {
		self: null,
	};
	var cssClass = {
		self: config.name.css('panel'),
		button: config.name.css('panel-button'),
	};
	var selector = {
		self: '.' + cssClass.self,
		button: '.' + cssClass.button,
	};

	//	██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████ ███████
	//	██ ████   ██ ██    ██    ██ ██   ██ ██      ██    ███  ██
	//	██ ██ ██  ██ ██    ██    ██ ███████ ██      ██   ███   █████
	//	██ ██  ██ ██ ██    ██    ██ ██   ██ ██      ██  ███    ██
	//	██ ██   ████ ██    ██    ██ ██   ██ ███████ ██ ███████ ███████
	// 
	// 	#initialize

	function initialize() {
		injectSvgSymbol(iconCog);
		injectSvgSymbol(iconToggle);
		injectSvgSymbol(iconCross);
		dom.self = document.querySelector(selector.self);
		if (dom.self === null) {
			throw new Error('Для панели быстрого доступа не найдены элементы в DOM');
		}
		dom.buttons = dom.self.querySelectorAll(selector.button);
		dom.buttons.forEach(function(element, id, list) {
			new Button(element);
		});
	}

	//	██   ██ ██ ██████  ███████
	//	██   ██ ██ ██   ██ ██
	//	███████ ██ ██   ██ █████
	//	██   ██ ██ ██   ██ ██
	//	██   ██ ██ ██████  ███████
	// 
	//	#hide

	function hide() {
		document.querySelector(config.name.cssClassSelector('panel')).classList.add(config.name.css('panel--hidden'));
	}

	//	██████  ██    ██ ████████ ████████  ██████  ███    ██
	//	██   ██ ██    ██    ██       ██    ██    ██ ████   ██
	//	██████  ██    ██    ██       ██    ██    ██ ██ ██  ██
	//	██   ██ ██    ██    ██       ██    ██    ██ ██  ██ ██
	//	██████   ██████     ██       ██     ██████  ██   ████
	// 
	//	#button

	function Button(target) {
		target.addEventListener('click', function(event) {
			event.preventDefault();
			// // var url = self.dom.self.href;
			// if (self.dom.self.getAttribute('href') != '#') {
			// 	modal.show({url: url, callback: hide});
			// }
		});
	}

	var panel = /*#__PURE__*/Object.freeze({
		initialize: initialize,
		hide: hide
	});

	function template(data) {
		var template =
`<div class="egml-admin_layer-editable_area egml-admin_layer-editable_area--hidden" title="Настроить область" style="font-size:${ 16 / data.parentFontSize }em">
	${ data.hasTitle ? `<div class="egml-admin_layer-editable_area-title">${ data.title }</div>` : '' }
</div>`	;
		return template;
	}

	// Список всех созданных областей
	var areas = [];

	// Индикатор видимости областей
	var shown = false;

	class Area {
		constructor(options) {
			// FIXME: Добавить индикатор области при выводе ошибки, чтобы понимать с какой областью проблема
			if (!options.selector) {
				return console.error('EditableArea: не определен селектор');
			}
			if (!options.url) {
				return console.error('EditableArea: не определен URL');
			}
			this.dom = {};
			this.dom.target = document.querySelector(options.selector);
			if (!this.dom.target) {
				return console.error('EditableArea: не найден элемент в DOM');
			}
			
			// Интервал обновления позиции и размера области
			this.repositionInterval = 1000;
			
			// HTML
			var templateData = {
				parentFontSize: config.rootFontSize,
				hasTitle: options.title ? true : false,
				title: options.title,
			};
			var fragment = document.createRange().createContextualFragment(template(templateData));
			this.dom.self = fragment.firstElementChild;
			document.body.appendChild(fragment);
			
			this.dom.self.addEventListener('click', function() {
				config.global.modal.show({
					url: options.url, 
					callback: function() {
						hide();
						toggle();
					},
				});
			});
			
			// Подстройка позиции и размеров области
			this.offset = {
				left: 0,
				top: 0,
				right: 0,
				bottom: 0,
			};
			if (options.offset) {
				let matches = options.offset.match(/(-?\d+)(?: (-?\d+)(?: (-?\d+)(?: (-?\d+))?)?)?/);
				let o1 = matches[1];
				let o2 = matches[2];
				let o3 = matches[3];
				let o4 = matches[4];
				if (o1 && !o2 && !o3 && !o4) {
					this.offset.left = 
					this.offset.top = 
					this.offset.right = 
					this.offset.bottom = Number(o1);
					// Можно еще так, но это только для целых чисел
					// this.offset.bottom = o1>>0;
				} else if (o1 && o2 && !o3 && !o4) {
					this.offset.top = 
					this.offset.bottom = Number(o1);
					this.offset.left = 
					this.offset.right = Number(o2);
				} else if (o1 && o2 && o3 && !o4) {
					this.offset.top = Number(o1);
					this.offset.left = 
					this.offset.right = Number(o2);
					this.offset.bottom = Number(o3);
				} else if (o1 && o2 && o3 && o4) {
					this.offset.top = Number(o1);
					this.offset.right = Number(o2);
					this.offset.bottom = Number(o3);
					this.offset.left = Number(o4);
				}
			}
			
			// Регистрация созданной области
			areas.push(this);
		}
		
		show() {
			this.dom.self.classList.remove('egml-admin_layer-editable_area--hidden');
			this.position();
			this.interval = window.setInterval(this.position.bind(this), this.repositionInterval);
		}
		
		hide() {
			this.dom.self.classList.add('egml-admin_layer-editable_area--hidden');
			window.clearInterval(this.interval);
			this.dom.self.style = null;
		}
		
		position() {
			var rect = this.dom.target.getBoundingClientRect();
			var scroll = pageScroll();
			this.dom.self.style.left = rect.left - 5 + scroll.left - this.offset.left + 'px';
			this.dom.self.style.top = rect.top - 5 + scroll.top - this.offset.top + 'px';
			this.dom.self.style.width = rect.width + 10 + this.offset.left + this.offset.right + 'px';
			this.dom.self.style.height = rect.height + 10 + this.offset.top + this.offset.bottom + 'px';
		}
	}

	//	 ██████ ██████  ███████  █████  ████████ ███████
	//	██      ██   ██ ██      ██   ██    ██    ██
	//	██      ██████  █████   ███████    ██    █████
	//	██      ██   ██ ██      ██   ██    ██    ██
	//	 ██████ ██   ██ ███████ ██   ██    ██    ███████
	// 
	//	#create
	function create(options) {
		new Area(options);
	}

	//	████████  ██████   ██████   ██████  ██      ███████
	//	   ██    ██    ██ ██       ██       ██      ██
	//	   ██    ██    ██ ██   ███ ██   ███ ██      █████
	//	   ██    ██    ██ ██    ██ ██    ██ ██      ██
	//	   ██     ██████   ██████   ██████  ███████ ███████
	// 
	//	#toggle
	function toggle() {
		if (areas.length) {
			areas.forEach(area => {
				shown ? area.hide() : area.show();
			});
			shown = !shown;
		} else {
			alert('Редактируемые области не определены на этой странице');
		}
	}

	var editableArea = /*#__PURE__*/Object.freeze({
		areas: areas,
		create: create,
		toggle: toggle
	});

	//	██    ██ ████████ ██ ██      ███████
	// import { hide as hideModal } from './modal';

	//	███████ ███████ ████████ ██    ██ ██████
	//	██      ██         ██    ██    ██ ██   ██
	//	███████ █████      ██    ██    ██ ██████
	//	     ██ ██         ██    ██    ██ ██
	//	███████ ███████    ██     ██████  ██
	// 
	//	██ ███    ██ ███    ██ ███████ ██████
	//	██ ████   ██ ████   ██ ██      ██   ██
	//	██ ██ ██  ██ ██ ██  ██ █████   ██████
	//	██ ██  ██ ██ ██  ██ ██ ██      ██   ██
	//	██ ██   ████ ██   ████ ███████ ██   ██
	// 
	//	██████   ██████   ██████ ██    ██ ███    ███ ███████ ███    ██ ████████
	//	██   ██ ██    ██ ██      ██    ██ ████  ████ ██      ████   ██    ██
	//	██   ██ ██    ██ ██      ██    ██ ██ ████ ██ █████   ██ ██  ██    ██
	//	██   ██ ██    ██ ██      ██    ██ ██  ██  ██ ██      ██  ██ ██    ██
	//	██████   ██████   ██████  ██████  ██      ██ ███████ ██   ████    ██
	// 
	//	#setup #inner #document
	function setupInnerDocument(innerDocument) {
		// При нажатии `Esc` закрывать окно слоя наложения
		innerDocument.addEventListener('keydown', function(event) {
			if (event.key ? event.key == 'Escape' : event.keyCode == 27) {
				config.global.modal.hide();
			}
		});
		 
		// Встраивание стиля во внутренний документ
		if (config.innerStyleUrl) {
			injectStyle({
				url: config.innerStyleUrl,
				targetDocument: innerDocument,
			});
		}
	}

	document.addEventListener('DOMContentLoaded', function() {
		config.global.modal.initialize();
		initialize();
	});

	exports.panel = panel;
	exports.editableArea = editableArea;
	exports.config = config;
	exports.setupInnerDocument = setupInnerDocument;

	return exports;

}({}));
