this.egml = this.egml || {};
this.egml.adminLayer = (function (exports) {
	'use strict';

	//  ██████  ███████  ██████  ██    ██ ███████ ███████ ████████

	//  ███████ ██   ██ ████████ ███████ ███    ██ ██████
	//  ██       ██ ██     ██    ██      ████   ██ ██   ██
	//  █████     ███      ██    █████   ██ ██  ██ ██   ██
	//  ██       ██ ██     ██    ██      ██  ██ ██ ██   ██
	//  ███████ ██   ██    ██    ███████ ██   ████ ██████
	//
	// #extend
	function extend(dest, source) {
		for (var prop in source) {
			dest[prop] = source[prop];
		}
		return dest;
	}

	//  ███████ ████████ ██████  ██ ███    ██  ██████
	//  ██         ██    ██   ██ ██ ████   ██ ██
	//  ███████    ██    ██████  ██ ██ ██  ██ ██   ███
	//       ██    ██    ██   ██ ██ ██  ██ ██ ██    ██
	//  ███████    ██    ██   ██ ██ ██   ████  ██████
	// 
	//   ██████  █████  ███████ ███████
	//  ██      ██   ██ ██      ██
	//  ██      ███████ ███████ █████
	//  ██      ██   ██      ██ ██
	//   ██████ ██   ██ ███████ ███████
	//
	// #string #case
	function stringCase(bits, type) {
		if (!bits || !bits.length) {
			return '';
		}
		if (typeof bits == 'string') {
			bits = [ bits ];
		}
		var string =  '';
		switch (type) {
			case 'camel':
				bits.forEach(function(bit, i) {
					if (i == 0) {
						string += firstLowerCase(bit);
					} else {
						string += firstUpperCase(bit);
					}
				});
				break;

			case 'capitalCamel':
				bits.forEach(function(bit) {
					string += firstUpperCase(bit);
				});
				break;

			case 'flat':
				string = bits.join('');
				break;

			case 'snake':
				string = bits.join('_');
				break;

			case 'kebab':
				string = bits.join('-');
				break;

			case 'dot':
				string = bits.join('.');
				break;
		
			case 'asIs':
			default:
				string = bits.join('');
				break;
		}
		return string;
	}

	//  ███████ ██ ██████  ███████ ████████
	//  ██      ██ ██   ██ ██         ██
	//  █████   ██ ██████  ███████    ██
	//  ██      ██ ██   ██      ██    ██
	//  ██      ██ ██   ██ ███████    ██
	// 
	//  ██    ██ ██████  ██████  ███████ ██████      ██
	//  ██    ██ ██   ██ ██   ██ ██      ██   ██    ██
	//  ██    ██ ██████  ██████  █████   ██████    ██
	//  ██    ██ ██      ██      ██      ██   ██  ██
	//   ██████  ██      ██      ███████ ██   ██ ██
	// 
	//  ██       ██████  ██     ██ ███████ ██████
	//  ██      ██    ██ ██     ██ ██      ██   ██
	//  ██      ██    ██ ██  █  ██ █████   ██████
	//  ██      ██    ██ ██ ███ ██ ██      ██   ██
	//  ███████  ██████   ███ ███  ███████ ██   ██
	// 
	//   ██████  █████  ███████ ███████
	//  ██      ██   ██ ██      ██
	//  ██      ███████ ███████ █████
	//  ██      ██   ██      ██ ██
	//   ██████ ██   ██ ███████ ███████
	//
	// #first #upper #lower #case
	function firstUpperCase(string) {
		return string[0].toUpperCase() + string.slice(1);
	}function firstLowerCase(string) {
		return string[0].toLowerCase() + string.slice(1);
	}
	// #TODO: Написать функционал логинга переменной или объекта в HTML
	// //  ██████   ██████  ███    ███
	// //  ██   ██ ██    ██ ████  ████
	// //  ██   ██ ██    ██ ██ ████ ██
	// //  ██   ██ ██    ██ ██  ██  ██
	// //  ██████   ██████  ██      ██
	// // 
	// //  ██       ██████   ██████
	// //  ██      ██    ██ ██
	// //  ██      ██    ██ ██   ███
	// //  ██      ██    ██ ██    ██
	// //  ███████  ██████   ██████
	// //
	// // #dom #log
	// export function domLog(object, target) {
	// 	if (typeof target == 'undefined') {
	// 		target = document;
	// 	}
	// 	var output = '';
	// 	if (typeof object == 'object') {
	// 		for (var key in object) {
	// 			if (object.hasOwnProperty(key)) {
	// 				var element = object[key];
	// 				output += '';
	// 			}
	// 		}
	// 	}
	// 	'<div style="font-family:monospace; font-size:1rem; white-space:nowrap;">' + 
	// 	'width &nbsp;= ' + rect.width + '<br>' +
	// 	'height = ' + rect.height + '<br>' +
	// 	'x &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= ' + rect.x + '<br>' +
	// 	'y &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;= ' + rect.y + '<br>' +
	// 	'left &nbsp;&nbsp;= ' + rect.left + '<br>' +
	// 	'top &nbsp;&nbsp;&nbsp;= ' + rect.top + '<br>' +
	// 	'right &nbsp;= ' + rect.right + '<br>' +
	// 	'bottom = ' + rect.bottom + '<br>' +
	// 	'</div>';
	// }

	//	██ ███    ██      ██ ███████  ██████ ████████
	//	██ ████   ██      ██ ██      ██         ██
	//	██ ██ ██  ██      ██ █████   ██         ██
	//	██ ██  ██ ██ ██   ██ ██      ██         ██
	//	██ ██   ████  █████  ███████  ██████    ██
	// 
	//	███████ ████████ ██    ██ ██      ███████
	//	██         ██     ██  ██  ██      ██
	//	███████    ██      ████   ██      █████
	//	     ██    ██       ██    ██      ██
	//	███████    ██       ██    ███████ ███████
	// 
	// #inject #style
	function injectStyle() {
		var options = {
			url: null,
			content: null,
			inline: null,
			mountElement: null,
			targetDocument: null,
			onLoadCallback: null,
		};
		
		var mountElement = null;
		
		options.inline = false;
		options.targetDocument = document;
		
		Object.defineProperty(options, 'mountElement', {
			get: function() {
				if (mountElement == null) {
					return options.targetDocument.head;
				} else {
					return mountElement;
				}
			},
			set: function(value) {
				mountElement = value;
			},
			configurable: true,
			enumerable: true,
		});
		
		if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
			options.url = arguments[0];
			options.onLoadCallback = arguments[1];
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			extend(options, arguments[0]);
		} else {
			return;
		}
		
		if (options.url) {
			if (options.inline == false) {
				var tag = options.targetDocument.createElement('link');
				tag.rel = 'stylesheet';
				if (typeof options.onLoadCallback == 'function') {
					tag.addEventListener('load', options.onLoadCallback);
				}
				tag.href = options.url;
				options.targetDocument.head.appendChild(tag);
			} else {
				var xhr = new XMLHttpRequest();
				xhr.open('get', options.url);
				xhr.onload = function() {
					var tag = options.targetDocument.createElement('style');
					tag.textContent = this.responseText;
					options.mountElement.appendChild(tag);
					if (typeof options.onLoadCallback == 'function') {
						options.onLoadCallback();
					}
				};
				xhr.send();
			}
		} else if (options.content) {
			var tag = options.targetDocument.createElement('style');
			tag.textContent = options.content;
			options.mountElement.appendChild(tag);
			if (typeof options.onLoadCallback == 'function') {
				options.onLoadCallback();
			}
		} else {
			return;
		}
	}

	//	██ ███    ██      ██ ███████  ██████ ████████
	//	██ ████   ██      ██ ██      ██         ██
	//	██ ██ ██  ██      ██ █████   ██         ██
	//	██ ██  ██ ██ ██   ██ ██      ██         ██
	//	██ ██   ████  █████  ███████  ██████    ██
	// 
	//	███████  ██████ ██████  ██ ██████  ████████
	//	██      ██      ██   ██ ██ ██   ██    ██
	//	███████ ██      ██████  ██ ██████     ██
	//	     ██ ██      ██   ██ ██ ██         ██
	//	███████  ██████ ██   ██ ██ ██         ██
	// 
	//	#inject #script
	function injectScript() {
		var options = {
			url: null,
			content: null,
			inline: null,
			mountElement: null,
			targetDocument: null,
			onLoadCallback: null,
		};

		var mountElement = null;

		options.inline = false;
		options.targetDocument = document;

		Object.defineProperty(options, 'mountElement', {
			get: function() {
				if (mountElement == null) {
					return options.targetDocument.head;
				} else {
					return mountElement;
				}
			},
			set: function(value) {
				mountElement = value;
			},
			configurable: true,
			enumerable: true,
		});

		if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
			options.url = arguments[0];
			options.onLoadCallback = arguments[1];
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			extend(options, arguments[0]);
		} else {
			return;
		}

		var tag = options.targetDocument.createElement('script');

		if (options.url) {
			if (options.inline == false) {
				if (typeof options.onLoadCallback == 'function') {
					tag.addEventListener('load', options.onLoadCallback);
				}
				tag.src = options.url;
				options.mountElement.appendChild(tag);
			} else {
				var xhr = new XMLHttpRequest();
				xhr.open('get', options.url);
				xhr.onload = function() {
					tag.textContent = this.responseText;
					options.mountElement.appendChild(tag);
					if (typeof options.onLoadCallback == 'function') {
						options.onLoadCallback();
					}
				};
				xhr.send();
			}
		} else if (options.content) {
			tag.textContent = options.content;
			options.mountElement.appendChild(tag);
			if (typeof options.onLoadCallback == 'function') {
				options.onLoadCallback();
			}
		} else {
			return;
		}
	}

	//	██ ███    ██      ██ ███████  ██████ ████████
	//	██ ████   ██      ██ ██      ██         ██
	//	██ ██ ██  ██      ██ █████   ██         ██
	//	██ ██  ██ ██ ██   ██ ██      ██         ██
	//	██ ██   ████  █████  ███████  ██████    ██
	// 
	//	██   ██ ████████ ███    ███ ██
	//	██   ██    ██    ████  ████ ██
	//	███████    ██    ██ ████ ██ ██
	//	██   ██    ██    ██  ██  ██ ██
	//	██   ██    ██    ██      ██ ███████
	// 
	//	#inject
	// export function injectHtml(url, inline, targetDocument, legacy) {}

	//	██ ███    ██      ██ ███████  ██████ ████████
	//	██ ████   ██      ██ ██      ██         ██
	//	██ ██ ██  ██      ██ █████   ██         ██
	//	██ ██  ██ ██ ██   ██ ██      ██         ██
	//	██ ██   ████  █████  ███████  ██████    ██
	// 
	//	███████ ██    ██  ██████
	//	██      ██    ██ ██
	//	███████ ██    ██ ██   ███
	//	     ██  ██  ██  ██    ██
	//	███████   ████    ██████
	// 
	//	███████ ██    ██ ███    ███ ██████   ██████  ██
	//	██       ██  ██  ████  ████ ██   ██ ██    ██ ██
	//	███████   ████   ██ ████ ██ ██████  ██    ██ ██
	//	     ██    ██    ██  ██  ██ ██   ██ ██    ██ ██
	//	███████    ██    ██      ██ ██████   ██████  ███████
	// 
	//	#inject #svg #symbol
	function injectSvgSymbol() {
		// -------------------------------------------
		//  Вариант с отдельным родительским
		// 	SVG-элементом для каждого символа:
		// -------------------------------------------
		var options = {
			symbol: null,
			mountElement: document.body,
			containerClass: null,
		};
		
		if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
			options.symbol = arguments[0];
			options.mountElement = arguments[1] || options.mountElement;
			options.containerClass = arguments[2] || options.containerClass;
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			extend(options, arguments[0]);
		} else {
			return;
		}

		var parser = new DOMParser();
		var symbolDocument = parser.parseFromString(
			`<svg xmlns="http://www.w3.org/2000/svg" 
			${ options.containerClass ? `class="${options.containerClass}"` : '' } 
			style="display: none;">
			${options.symbol}
		</svg>`,
			'image/svg+xml'
		);
		var symbolNode = document.importNode(symbolDocument.documentElement, true);
		options.mountElement.appendChild(symbolNode);

		// -------------------------------------------
		//  Вариант со вставкой в один родительский 
		// 	SVG-элемент:
		// -------------------------------------------
		// var options = {
		// 	symbol: null,
		// 	containerId: 'svg_sprite',
		// 	mountElement: document.body,
		// };
		 
		// if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
		// 	options.symbol = arguments[0];
		// 	options.containerId = arguments[1] || options.containerId;
		// 	options.mountElement = arguments[2] || options.mountElement;
		// } else if (!!arguments[0] && typeof arguments[0] == 'object') {
		// 	extend(options, arguments[0]);
		// } else {
		// 	return;
		// }

		// var container = document.getElementById(options.containerId);
		
		// if (container == null) {
		// 	container = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		// 	container.setAttribute('id', options.containerId);
		// 	container.style.cssText = 'display: none;';
		// 	options.mountElement.appendChild(container);
		// }

		// var parser = new DOMParser();
		// var symbolDocument = parser.parseFromString(
		// 	`<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
		// 		${options.symbol}
		// 	</svg>`,
		// 	'image/svg+xml'
		// );
		// var symbolNode = document.importNode(symbolDocument.querySelector('symbol'), true);
		// container.appendChild(symbolNode);
	}

	function StringCase(bits) {
		this.bits = bits;

		if (Array.isArray(bits)) {
			this.asIs = this.bits.join('');
			bits.forEach(function(bit, i) {
				bits[i] = bit.toLowerCase();
			});
		} else {
			this.asIs = this.bits;
			bits = bits.toLowerCase();
		}

		this.camel = stringCase(bits, 'camel');
		this.capitalCamel = stringCase(bits, 'capitalCamel');
		this.kebab = stringCase(bits, 'kebab');
		this.snake = stringCase(bits, 'snake');
		this.flat = stringCase(bits, 'flat');
		this.dot = stringCase(bits, 'dot');
	}
	StringCase.prototype.toString = function() {
		return this.asIs;
	};

	function Name(nameBits, nsBits) {
		var bits;

		// Далее идет обработка параметра nsBits:
		// - если он не задан, пропустить все и взять ns из прототипа
		// - если он boolean и true, то также пропустить и использовать ns 
		//   из прототипа
		// - если он boolean и false, то nsBits - пустая строка
		// - и если в результате nsBits строка или массив, то создать новое 
		//   свойство ns поверх геттера прототипа со значением nsBits
		if (nsBits != null) {
			if (typeof nsBits == 'boolean' && nsBits == false) {
				nsBits = '';
			}
			if (typeof nsBits == 'string' || Array.isArray(nsBits)) {
				Object.defineProperty(this, 'ns', {
					value: new StringCase(nsBits),
					writable: true,
					configurable: true,
					enumerable: true,
				});
			}
		}

		var hasNs = this.ns && this.ns.toString() != '';

		this.base = new StringCase(nameBits);

		// #asIs #flat #camel #capital #kebab
		var types = ['asIs', 'flat', 'camel', 'capitalCamel', 'kebab'];
		for (var i = 0; i < types.length; i++) {
			if (hasNs) {
				this['_' + types[i]] = stringCase([this.ns[types[i]], this.base[types[i]]], types[i]);
			} else {
				this['_' + types[i]] = this.base[types[i]];
			}
			this[types[i]] = (function(type, nameObj) {
				return function(suffix) {
					if (suffix) {
						if (type != 'asIs') {
							if (Array.isArray(suffix)) {
								suffix.forEach(function(bit, i) {
									suffix[i] = bit.toLowerCase();
								});
							}
						}
						return stringCase([nameObj['_' + type], stringCase(suffix, type)], type);
					} else {
						return nameObj['_' + type];
					}
				};
			})(types[i], this);
		}

		//  ██████   ██████  ████████
		//  ██   ██ ██    ██    ██
		//  ██   ██ ██    ██    ██
		//  ██   ██ ██    ██    ██
		//  ██████   ██████     ██
		//
		// #dot
		var bits = [ this.base.camel ];
		if (hasNs) {
			bits.unshift(this.ns.camel);
		}
		this._dot = stringCase(bits, 'dot');
		this.dot = function(suffix) {
			if (suffix) {
				if (Array.isArray(suffix)) {
					suffix.forEach(function(bit, i) {
						suffix[i] = bit.toLowerCase();
					});
				}
				return stringCase([this._dot, stringCase(suffix, 'camel')], 'dot');
			} else {
				return this._dot;
			}
		};

		//  ███████ ██    ██ ███████ ███    ██ ████████
		//  ██      ██    ██ ██      ████   ██    ██
		//  █████   ██    ██ █████   ██ ██  ██    ██
		//  ██       ██  ██  ██      ██  ██ ██    ██
		//  ███████   ████   ███████ ██   ████    ██
		//
		// #event
		this.event = this.dot;

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		//
		// #css
		bits = [ this.base.snake ];
		if (hasNs) {
			bits.unshift(this.ns.snake);
		}
		this._css = stringCase(bits, 'kebab');
		this.css = function(suffix) {
			if (suffix) {
				if (Array.isArray(suffix)) {
					suffix.forEach(function(bit, i) {
						suffix[i] = bit.toLowerCase();
					});
				}
				return stringCase([this._css, stringCase(suffix, 'snake')], 'kebab');
			} else {
				return this._css;
			}
		};

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		// 
		//  ███    ███  ██████  ██████
		//  ████  ████ ██    ██ ██   ██
		//  ██ ████ ██ ██    ██ ██   ██
		//  ██  ██  ██ ██    ██ ██   ██
		//  ██      ██  ██████  ██████
		//
		// #css #modificator
		this.cssModificator = function(modificatorName) {
			if (modificatorName != null) {
				if (Array.isArray(modificatorName)) {
					modificatorName.forEach(function(bit, i) {
						modificatorName[i] = bit.toLowerCase();
					});
				}
				return this.css() + '--' + stringCase(modificatorName, 'snake');
			} else {
				return this.css();
			}
		};
		this.cssMod = this.cssModificator;

		//   ██████ ███████ ███████
		//  ██      ██      ██
		//  ██      ███████ ███████
		//  ██           ██      ██
		//   ██████ ███████ ███████
		// 
		//   ██████ ██       █████  ███████ ███████
		//  ██      ██      ██   ██ ██      ██
		//  ██      ██      ███████ ███████ ███████
		//  ██      ██      ██   ██      ██      ██
		//   ██████ ███████ ██   ██ ███████ ███████
		// 
		//  ███████ ███████ ██      ███████  ██████ ████████  ██████  ██████
		//  ██      ██      ██      ██      ██         ██    ██    ██ ██   ██
		//  ███████ █████   ██      █████   ██         ██    ██    ██ ██████
		//       ██ ██      ██      ██      ██         ██    ██    ██ ██   ██
		//  ███████ ███████ ███████ ███████  ██████    ██     ██████  ██   ██
		//
		// #css #class #selector
		this.cssClassSelector = function(appendage) {
			return '.' + this.css(appendage);
		};
		this.selector = function(appendage) {
			return this.cssClassSelector(appendage);
		};
	}
	var ns;
	Object.defineProperty(Name.prototype, 'ns', {
		set: function(value) {
			ns = new StringCase(value);
		},
		get: function() {
			return ns;
		},
	});
	Name.prototype.toString = function() {
		return this._asIs;
	};

	var pkg = {name:"@egml/admin-layer",version:"1.0.0","private":true,egml:{name:["admin","layer"],namespace:"egml"},sideEffects:["*.scss","*.svg"],scripts:{js:"rollup --config","js-watch":"rollup --config --watch","js-webpack":"webpack --config-name demo","js-webpack-watch":"webpack --config-name demo --watch","js-build":"rollup --config build.rollup.config.js",css:"grunt sass:demo","css-watch":"grunt watch:css","css-build":"grunt sass:dist",clean:"grunt clean:demo","clean-js":"grunt clean:js","clean-css":"grunt clean:css","clean-svg":"grunt clean:svg","clean-build":"grunt clean:dist",svg:"grunt svg",build:"yarn clean-build && grunt build && yarn js-build"},dependencies:{"@egml/utils":"https://bitbucket.org/egml/utils",jquery:"^3.3.1","jquery.transit":"https://github.com/egml/jquery.transit","spin.js":"^2.3.2"},devDependencies:{"@babel/core":"^7.3.4","@babel/plugin-syntax-dynamic-import":"^7.2.0","@babel/polyfill":"^7.2.5","@babel/preset-env":"^7.3.4","babel-loader":"^8.0.5","cookies.js":"git+https://github.com/madmurphy/cookies.js.git","css-loader":"^2.1.0",grunt:"^1.0.3","grunt-concurrent":"^2.3.1","grunt-contrib-clean":"^2.0.0","grunt-contrib-copy":"^1.0.0","grunt-contrib-imagemin":"^3.1.0","grunt-contrib-watch":"^1.1.0","grunt-newer":"^1.3.0","grunt-notify":"^0.4.5","grunt-sass":"^3.0.2","grunt-svgstore":"^2.0.0","handlebars-loader":"^1.7.1","html-webpack-plugin":"^4.0.0-beta.5","load-grunt-tasks":"^4.0.0","module-alias":"^2.2.0","mustache-loader":"^1.4.3","node-sass":"^4.11.0","node-sass-import":"^2.0.1","normalize-scss":"^7.0.1","normalize.css":"^8.0.1","pug-loader":"^2.4.0",rollup:"^1.2.2","rollup-plugin-butternut":"^0.1.0","rollup-plugin-commonjs":"^9.2.1","rollup-plugin-json":"^3.1.0","rollup-plugin-mustache":"^0.1.0","rollup-plugin-node-resolve":"^4.0.1","rollup-plugin-progress":"^1.0.0","rollup-plugin-pug":"^1.1.0","rollup-plugin-sass":"^1.1.0","rollup-plugin-sizes":"^0.5.1","rollup-plugin-string":"^3.0.0","rollup-plugin-svg-to-symbol":"^1.0.0","rollup-plugin-svgo":"^1.0.2",sass:"^1.16.0","sass-loader":"^7.1.0","style-loader":"^0.23.1","svg-inline-loader":"^0.8.0","svg-sprite-loader":"^4.1.3","svgo-loader":"^2.2.0","url-loader":"^1.1.2",webpack:"^4.28.4","webpack-bundle-analyzer":"^3.1.0","webpack-cli":"^3.2.1","webpack-notifier":"^1.7.0"}};

	//	 ██████  ██████  ███    ██ ███████ ██  ██████

	Name.prototype.ns = pkg.egml.namespace;
	 
	var config = {
		name: new Name(pkg.egml.name),
		publicPath: null,
		rootFontSize: 16,
		innerStyleUrl: null,
	};
	  
	Object.defineProperty(config, 'global', {
		get: function() {
			return window[config.name.ns.camel][config.name.base.camel];
		}
	});

	var iconLock = "<symbol id=\"egml-admin_layer-svg_sprite-lock\" viewBox=\"0 0 100 100\"><path d=\"M 50,0 C 33.564856,0 20.153675,13.411181 20.153675,29.846327 v 12.143715 h -5.140368 c -4.665586,0 -8.4199,3.75646 -8.4199,8.422046 v 41.165865 c 0,4.665585 3.754314,8.422047 8.4199,8.422047 h 69.973386 c 4.665585,0 8.4199,-3.756462 8.4199,-8.422047 V 50.412087 c 0,-4.665585 -3.754315,-8.422046 -8.4199,-8.422046 H 79.846325 V 29.846327 C 79.846325,13.411181 66.435145,0 50,0 Z m 0,11.538461 c 10.24242,0 18.307863,8.065444 18.307863,18.307866 V 41.990042 H 31.692137 V 29.846327 C 31.692137,19.603905 39.75758,11.538461 50,11.538461 Z m 0,43.902389 a 8.0728139,8.0683019 0 0 1 8.072201,8.070053 8.0728139,8.0683019 0 0 1 -2.30297,5.631867 v 11.639339 c 0,3.194365 -2.573077,5.767084 -5.769231,5.767084 -3.196154,0 -5.769231,-2.572719 -5.769231,-5.767084 V 69.151354 A 8.0728139,8.0683019 0 0 1 41.927799,63.510903 8.0728139,8.0683019 0 0 1 50,55.44085 Z\"/></symbol>";

	//	██    ██ ████████ ██ ██      ███████
	// import { hide as hideModal } from './modal';

	//	███████ ███████ ████████ ██    ██ ██████
	//	██      ██         ██    ██    ██ ██   ██
	//	███████ █████      ██    ██    ██ ██████
	//	     ██ ██         ██    ██    ██ ██
	//	███████ ███████    ██     ██████  ██
	// 
	//	██ ███    ██ ███    ██ ███████ ██████
	//	██ ████   ██ ████   ██ ██      ██   ██
	//	██ ██ ██  ██ ██ ██  ██ █████   ██████
	//	██ ██  ██ ██ ██  ██ ██ ██      ██   ██
	//	██ ██   ████ ██   ████ ███████ ██   ██
	// 
	//	██████   ██████   ██████ ██    ██ ███    ███ ███████ ███    ██ ████████
	//	██   ██ ██    ██ ██      ██    ██ ████  ████ ██      ████   ██    ██
	//	██   ██ ██    ██ ██      ██    ██ ██ ████ ██ █████   ██ ██  ██    ██
	//	██   ██ ██    ██ ██      ██    ██ ██  ██  ██ ██      ██  ██ ██    ██
	//	██████   ██████   ██████  ██████  ██      ██ ███████ ██   ████    ██
	// 
	//	#setup #inner #document
	function setupInnerDocument(innerDocument) {
		// При нажатии `Esc` закрывать окно слоя наложения
		innerDocument.addEventListener('keydown', function(event) {
			if (event.key ? event.key == 'Escape' : event.keyCode == 27) {
				config.global.modal.hide();
			}
		});
		 
		// Встраивание стиля во внутренний документ
		if (config.innerStyleUrl) {
			injectStyle({
				url: config.innerStyleUrl,
				targetDocument: innerDocument,
			});
		}
	}

	var busy = false;
	var loaded = false;

	document.addEventListener('DOMContentLoaded', function() {
		injectSvgSymbol(iconLock);
		var accessPoints = document.querySelectorAll(config.name.cssClassSelector('access_point'));
		if (accessPoints.length) {
			for (var i = 0; i < accessPoints.length; i++) {
				accessPoints[i].addEventListener('click', function(event) {
					if (busy || loaded) return;
					event.preventDefault();
					var accessPoint = this;
					var classLoading = config.name.css('access_point--loading');
					busy = true;
					accessPoint.classList.add(classLoading);
					injectStyle(config.publicPath + config.name.kebab('modal.css'), function() {
						injectScript(config.publicPath + config.name.kebab('modal-html.js'), function() {
							injectScript(config.publicPath + config.name.kebab('modal.js'), function() {
								loaded = true;
								accessPoint.classList.remove(classLoading);
								config.global.modal.initialize();
								config.global.modal.show({ url: accessPoint.href });
							});
						});
					});
				}, false);
			}
		}
	});

	exports.config = config;
	exports.setupInnerDocument = setupInnerDocument;

	return exports;

}({}));
