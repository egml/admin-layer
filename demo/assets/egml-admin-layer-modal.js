this.egml = this.egml || {};
this.egml.adminLayer = this.egml.adminLayer || {};
this.egml.adminLayer.modal = (function (exports, config) {
	'use strict';

	config = config && config.hasOwnProperty('default') ? config['default'] : config;

	var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var spin = createCommonjsModule(function (module) {
	(function (root, factory) {

	  /* CommonJS */
	  if (module.exports) module.exports = factory();

	  /* AMD module */
	  else root.Spinner = factory();
	}(commonjsGlobal, function () {

	  var prefixes = ['webkit', 'Moz', 'ms', 'O'] /* Vendor prefixes */
	    , animations = {} /* Animation rules keyed by their name */
	    , useCssAnimations /* Whether to use CSS animations or setTimeout */
	    , sheet; /* A stylesheet to hold the @keyframe or VML rules. */

	  /**
	   * Utility function to create elements. If no tag name is given,
	   * a DIV is created. Optionally properties can be passed.
	   */
	  function createEl (tag, prop) {
	    var el = document.createElement(tag || 'div')
	      , n;

	    for (n in prop) el[n] = prop[n];
	    return el
	  }

	  /**
	   * Appends children and returns the parent.
	   */
	  function ins (parent /* child1, child2, ...*/) {
	    for (var i = 1, n = arguments.length; i < n; i++) {
	      parent.appendChild(arguments[i]);
	    }

	    return parent
	  }

	  /**
	   * Creates an opacity keyframe animation rule and returns its name.
	   * Since most mobile Webkits have timing issues with animation-delay,
	   * we create separate rules for each line/segment.
	   */
	  function addAnimation (alpha, trail, i, lines) {
	    var name = ['opacity', trail, ~~(alpha * 100), i, lines].join('-')
	      , start = 0.01 + i/lines * 100
	      , z = Math.max(1 - (1-alpha) / trail * (100-start), alpha)
	      , prefix = useCssAnimations.substring(0, useCssAnimations.indexOf('Animation')).toLowerCase()
	      , pre = prefix && '-' + prefix + '-' || '';

	    if (!animations[name]) {
	      sheet.insertRule(
	        '@' + pre + 'keyframes ' + name + '{' +
	        '0%{opacity:' + z + '}' +
	        start + '%{opacity:' + alpha + '}' +
	        (start+0.01) + '%{opacity:1}' +
	        (start+trail) % 100 + '%{opacity:' + alpha + '}' +
	        '100%{opacity:' + z + '}' +
	        '}', sheet.cssRules.length);

	      animations[name] = 1;
	    }

	    return name
	  }

	  /**
	   * Tries various vendor prefixes and returns the first supported property.
	   */
	  function vendor (el, prop) {
	    var s = el.style
	      , pp
	      , i;

	    prop = prop.charAt(0).toUpperCase() + prop.slice(1);
	    if (s[prop] !== undefined) return prop
	    for (i = 0; i < prefixes.length; i++) {
	      pp = prefixes[i]+prop;
	      if (s[pp] !== undefined) return pp
	    }
	  }

	  /**
	   * Sets multiple style properties at once.
	   */
	  function css (el, prop) {
	    for (var n in prop) {
	      el.style[vendor(el, n) || n] = prop[n];
	    }

	    return el
	  }

	  /**
	   * Fills in default values.
	   */
	  function merge (obj) {
	    for (var i = 1; i < arguments.length; i++) {
	      var def = arguments[i];
	      for (var n in def) {
	        if (obj[n] === undefined) obj[n] = def[n];
	      }
	    }
	    return obj
	  }

	  /**
	   * Returns the line color from the given string or array.
	   */
	  function getColor (color, idx) {
	    return typeof color == 'string' ? color : color[idx % color.length]
	  }

	  // Built-in defaults

	  var defaults = {
	    lines: 12             // The number of lines to draw
	  , length: 7             // The length of each line
	  , width: 5              // The line thickness
	  , radius: 10            // The radius of the inner circle
	  , scale: 1.0            // Scales overall size of the spinner
	  , corners: 1            // Roundness (0..1)
	  , color: '#000'         // #rgb or #rrggbb
	  , opacity: 1/4          // Opacity of the lines
	  , rotate: 0             // Rotation offset
	  , direction: 1          // 1: clockwise, -1: counterclockwise
	  , speed: 1              // Rounds per second
	  , trail: 100            // Afterglow percentage
	  , fps: 20               // Frames per second when using setTimeout()
	  , zIndex: 2e9           // Use a high z-index by default
	  , className: 'spinner'  // CSS class to assign to the element
	  , top: '50%'            // center vertically
	  , left: '50%'           // center horizontally
	  , shadow: false         // Whether to render a shadow
	  , hwaccel: false        // Whether to use hardware acceleration (might be buggy)
	  , position: 'absolute'  // Element positioning
	  };

	  /** The constructor */
	  function Spinner (o) {
	    this.opts = merge(o || {}, Spinner.defaults, defaults);
	  }

	  // Global defaults that override the built-ins:
	  Spinner.defaults = {};

	  merge(Spinner.prototype, {
	    /**
	     * Adds the spinner to the given target element. If this instance is already
	     * spinning, it is automatically removed from its previous target b calling
	     * stop() internally.
	     */
	    spin: function (target) {
	      this.stop();

	      var self = this
	        , o = self.opts
	        , el = self.el = createEl(null, {className: o.className});

	      css(el, {
	        position: o.position
	      , width: 0
	      , zIndex: o.zIndex
	      , left: o.left
	      , top: o.top
	      });

	      if (target) {
	        target.insertBefore(el, target.firstChild || null);
	      }

	      el.setAttribute('role', 'progressbar');
	      self.lines(el, self.opts);

	      if (!useCssAnimations) {
	        // No CSS animation support, use setTimeout() instead
	        var i = 0
	          , start = (o.lines - 1) * (1 - o.direction) / 2
	          , alpha
	          , fps = o.fps
	          , f = fps / o.speed
	          , ostep = (1 - o.opacity) / (f * o.trail / 100)
	          , astep = f / o.lines

	        ;(function anim () {
	          i++;
	          for (var j = 0; j < o.lines; j++) {
	            alpha = Math.max(1 - (i + (o.lines - j) * astep) % f * ostep, o.opacity);

	            self.opacity(el, j * o.direction + start, alpha, o);
	          }
	          self.timeout = self.el && setTimeout(anim, ~~(1000 / fps));
	        })();
	      }
	      return self
	    }

	    /**
	     * Stops and removes the Spinner.
	     */
	  , stop: function () {
	      var el = this.el;
	      if (el) {
	        clearTimeout(this.timeout);
	        if (el.parentNode) el.parentNode.removeChild(el);
	        this.el = undefined;
	      }
	      return this
	    }

	    /**
	     * Internal method that draws the individual lines. Will be overwritten
	     * in VML fallback mode below.
	     */
	  , lines: function (el, o) {
	      var i = 0
	        , start = (o.lines - 1) * (1 - o.direction) / 2
	        , seg;

	      function fill (color, shadow) {
	        return css(createEl(), {
	          position: 'absolute'
	        , width: o.scale * (o.length + o.width) + 'px'
	        , height: o.scale * o.width + 'px'
	        , background: color
	        , boxShadow: shadow
	        , transformOrigin: 'left'
	        , transform: 'rotate(' + ~~(360/o.lines*i + o.rotate) + 'deg) translate(' + o.scale*o.radius + 'px' + ',0)'
	        , borderRadius: (o.corners * o.scale * o.width >> 1) + 'px'
	        })
	      }

	      for (; i < o.lines; i++) {
	        seg = css(createEl(), {
	          position: 'absolute'
	        , top: 1 + ~(o.scale * o.width / 2) + 'px'
	        , transform: o.hwaccel ? 'translate3d(0,0,0)' : ''
	        , opacity: o.opacity
	        , animation: useCssAnimations && addAnimation(o.opacity, o.trail, start + i * o.direction, o.lines) + ' ' + 1 / o.speed + 's linear infinite'
	        });

	        if (o.shadow) ins(seg, css(fill('#000', '0 0 4px #000'), {top: '2px'}));
	        ins(el, ins(seg, fill(getColor(o.color, i), '0 0 1px rgba(0,0,0,.1)')));
	      }
	      return el
	    }

	    /**
	     * Internal method that adjusts the opacity of a single line.
	     * Will be overwritten in VML fallback mode below.
	     */
	  , opacity: function (el, i, val) {
	      if (i < el.childNodes.length) el.childNodes[i].style.opacity = val;
	    }

	  });


	  function initVML () {

	    /* Utility function to create a VML tag */
	    function vml (tag, attr) {
	      return createEl('<' + tag + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', attr)
	    }

	    // No CSS transforms but VML support, add a CSS rule for VML elements:
	    sheet.addRule('.spin-vml', 'behavior:url(#default#VML)');

	    Spinner.prototype.lines = function (el, o) {
	      var r = o.scale * (o.length + o.width)
	        , s = o.scale * 2 * r;

	      function grp () {
	        return css(
	          vml('group', {
	            coordsize: s + ' ' + s
	          , coordorigin: -r + ' ' + -r
	          })
	        , { width: s, height: s }
	        )
	      }

	      var margin = -(o.width + o.length) * o.scale * 2 + 'px'
	        , g = css(grp(), {position: 'absolute', top: margin, left: margin})
	        , i;

	      function seg (i, dx, filter) {
	        ins(
	          g
	        , ins(
	            css(grp(), {rotation: 360 / o.lines * i + 'deg', left: ~~dx})
	          , ins(
	              css(
	                vml('roundrect', {arcsize: o.corners})
	              , { width: r
	                , height: o.scale * o.width
	                , left: o.scale * o.radius
	                , top: -o.scale * o.width >> 1
	                , filter: filter
	                }
	              )
	            , vml('fill', {color: getColor(o.color, i), opacity: o.opacity})
	            , vml('stroke', {opacity: 0}) // transparent stroke to fix color bleeding upon opacity change
	            )
	          )
	        );
	      }

	      if (o.shadow)
	        for (i = 1; i <= o.lines; i++) {
	          seg(i, -2, 'progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)');
	        }

	      for (i = 1; i <= o.lines; i++) seg(i);
	      return ins(el, g)
	    };

	    Spinner.prototype.opacity = function (el, i, val, o) {
	      var c = el.firstChild;
	      o = o.shadow && o.lines || 0;
	      if (c && i + o < c.childNodes.length) {
	        c = c.childNodes[i + o]; c = c && c.firstChild; c = c && c.firstChild;
	        if (c) c.opacity = val;
	      }
	    };
	  }

	  if (typeof document !== 'undefined') {
	    sheet = (function () {
	      var el = createEl('style', {type : 'text/css'});
	      ins(document.getElementsByTagName('head')[0], el);
	      return el.sheet || el.styleSheet
	    }());

	    var probe = css(createEl('group'), {behavior: 'url(#default#VML)'});

	    if (!vendor(probe, 'transform') && probe.adj) initVML();
	    else useCssAnimations = vendor(probe, 'animation');
	  }

	  return Spinner

	}));
	});

	var opts = {
	  lines: 13 // The number of lines to draw
	, length: 6 // The length of each line
	, width: 2 // The line thickness
	, radius: 11 // The radius of the inner circle
	, scale: 1 // Scales overall size of the spinner
	, corners: 0 // Corner roundness (0..1)
	, color: '#000' // #rgb or #rrggbb or array of colors
	, opacity: 0.25 // Opacity of the lines
	, rotate: 0 // The rotation offset
	, direction: 1 // 1: clockwise, -1: counterclockwise
	, speed: 1 // Rounds per second
	, trail: 60 // Afterglow percentage
	, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
	, zIndex: 2e9 // The z-index (defaults to 2000000000)
	, className: 'spinner' // The CSS class to assign to the spinner
	, top: '50%' // Top position relative to parent
	, left: '50%' // Left position relative to parent
	, shadow: false // Whether to render a shadow
	, hwaccel: false // Whether to use hardware acceleration
	, position: 'absolute' // Element positioning
	};

	var Spinner = new spin(opts);

	var scroll;

	function crop() {
		if (window.scrollX || window.scrollY) {
			scroll = {
				top: window.scrollY,
				left: window.scrollX,
			};
		}
		document.documentElement.style.overflow = 'hidden';
		//document.body.style.overflow = 'hidden';
	}
	function restore() {
		document.documentElement.style.overflow = null;
		//document.body.style.overflow = null;
	}

	var iconCross = "<symbol id=\"egml-admin_layer-svg_sprite-cross\" viewBox=\"0 0 100 100\"><path d=\"M 20,80 80,20\"/><path d=\"M 20,20 80,80\"/></symbol>";

	//  ██████  ███████  ██████  ██    ██ ███████ ███████ ████████

	//  ███████ ██   ██ ████████ ███████ ███    ██ ██████
	//  ██       ██ ██     ██    ██      ████   ██ ██   ██
	//  █████     ███      ██    █████   ██ ██  ██ ██   ██
	//  ██       ██ ██     ██    ██      ██  ██ ██ ██   ██
	//  ███████ ██   ██    ██    ███████ ██   ████ ██████
	//
	// #extend
	function extend(dest, source) {
		for (var prop in source) {
			dest[prop] = source[prop];
		}
		return dest;
	}

	//  ██████  ███████ ███    ███
	//  ██   ██ ██      ████  ████
	//  ██████  █████   ██ ████ ██
	//  ██   ██ ██      ██  ██  ██
	//  ██   ██ ███████ ██      ██
	//
	// #rem
	// Подсчёт величины в rem. На входе пиксели без `px`, на выходе величина в rem с `rem`
	var rootFontSize;
	function rem(pxNoUnits, recalc) {
		if (typeof rootFontSize != 'number' || recalc) {
			// #CBFIX: Edge (42.17134.1.0, EdgeHTML 17.17134) и IE определяют значение как, например, 9.93 в результате >> операция дает 9, поэтому тут дополнительно надо округлять. UPD: Зачем переводить в integer с помощью >>, если мы уже применям к строке Math.round, тем самым она автоматически кастуется в integer.
			// rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0,-2)) >> 0;
			rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0,-2));
		}
		return pxNoUnits/rootFontSize + 'rem';
	}

	//  ███    ███  █████  ██████   ██████  ██ ███    ██
	//  ████  ████ ██   ██ ██   ██ ██       ██ ████   ██
	//  ██ ████ ██ ███████ ██████  ██   ███ ██ ██ ██  ██
	//  ██  ██  ██ ██   ██ ██   ██ ██    ██ ██ ██  ██ ██
	//  ██      ██ ██   ██ ██   ██  ██████  ██ ██   ████
	// 
	//  ██████   ██████  ██   ██
	//  ██   ██ ██    ██  ██ ██
	//  ██████  ██    ██   ███
	//  ██   ██ ██    ██  ██ ██
	//  ██████   ██████  ██   ██
	// 
	//  ██   ██ ███████ ██  ██████  ██   ██ ████████
	//  ██   ██ ██      ██ ██       ██   ██    ██
	//  ███████ █████   ██ ██   ███ ███████    ██
	//  ██   ██ ██      ██ ██    ██ ██   ██    ██
	//  ██   ██ ███████ ██  ██████  ██   ██    ██
	//
	// #margin #box #height
	function marginBoxHeight(element, sourceWindow) {
		sourceWindow = sourceWindow || window;
		var style;
		if (element && (style = sourceWindow.getComputedStyle(element))) {
			return element.getBoundingClientRect().height
					+ (style.getPropertyValue('margin-top').slice(0,-2) >> 0)
					+ (style.getPropertyValue('margin-bottom').slice(0,-2) >> 0);
		} else {
			return 0;
		}
	}

	//	██ ███    ██      ██ ███████  ██████ ████████
	//	██ ████   ██      ██ ██      ██         ██
	//	██ ██ ██  ██      ██ █████   ██         ██
	//	██ ██  ██ ██ ██   ██ ██      ██         ██
	//	██ ██   ████  █████  ███████  ██████    ██
	// 
	//	██   ██ ████████ ███    ███ ██
	//	██   ██    ██    ████  ████ ██
	//	███████    ██    ██ ████ ██ ██
	//	██   ██    ██    ██  ██  ██ ██
	//	██   ██    ██    ██      ██ ███████
	// 
	//	#inject
	// export function injectHtml(url, inline, targetDocument, legacy) {}

	//	██ ███    ██      ██ ███████  ██████ ████████
	//	██ ████   ██      ██ ██      ██         ██
	//	██ ██ ██  ██      ██ █████   ██         ██
	//	██ ██  ██ ██ ██   ██ ██      ██         ██
	//	██ ██   ████  █████  ███████  ██████    ██
	// 
	//	███████ ██    ██  ██████
	//	██      ██    ██ ██
	//	███████ ██    ██ ██   ███
	//	     ██  ██  ██  ██    ██
	//	███████   ████    ██████
	// 
	//	███████ ██    ██ ███    ███ ██████   ██████  ██
	//	██       ██  ██  ████  ████ ██   ██ ██    ██ ██
	//	███████   ████   ██ ████ ██ ██████  ██    ██ ██
	//	     ██    ██    ██  ██  ██ ██   ██ ██    ██ ██
	//	███████    ██    ██      ██ ██████   ██████  ███████
	// 
	//	#inject #svg #symbol
	function injectSvgSymbol() {
		// -------------------------------------------
		//  Вариант с отдельным родительским
		// 	SVG-элементом для каждого символа:
		// -------------------------------------------
		var options = {
			symbol: null,
			mountElement: document.body,
			containerClass: null,
		};
		
		if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
			options.symbol = arguments[0];
			options.mountElement = arguments[1] || options.mountElement;
			options.containerClass = arguments[2] || options.containerClass;
		} else if (!!arguments[0] && typeof arguments[0] == 'object') {
			extend(options, arguments[0]);
		} else {
			return;
		}

		var parser = new DOMParser();
		var symbolDocument = parser.parseFromString(
			`<svg xmlns="http://www.w3.org/2000/svg" 
			${ options.containerClass ? `class="${options.containerClass}"` : '' } 
			style="display: none;">
			${options.symbol}
		</svg>`,
			'image/svg+xml'
		);
		var symbolNode = document.importNode(symbolDocument.documentElement, true);
		options.mountElement.appendChild(symbolNode);

		// -------------------------------------------
		//  Вариант со вставкой в один родительский 
		// 	SVG-элемент:
		// -------------------------------------------
		// var options = {
		// 	symbol: null,
		// 	containerId: 'svg_sprite',
		// 	mountElement: document.body,
		// };
		 
		// if (typeof arguments[0] == 'string' || arguments[0] instanceof String) {
		// 	options.symbol = arguments[0];
		// 	options.containerId = arguments[1] || options.containerId;
		// 	options.mountElement = arguments[2] || options.mountElement;
		// } else if (!!arguments[0] && typeof arguments[0] == 'object') {
		// 	extend(options, arguments[0]);
		// } else {
		// 	return;
		// }

		// var container = document.getElementById(options.containerId);
		
		// if (container == null) {
		// 	container = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		// 	container.setAttribute('id', options.containerId);
		// 	container.style.cssText = 'display: none;';
		// 	options.mountElement.appendChild(container);
		// }

		// var parser = new DOMParser();
		// var symbolDocument = parser.parseFromString(
		// 	`<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
		// 		${options.symbol}
		// 	</svg>`,
		// 	'image/svg+xml'
		// );
		// var symbolNode = document.importNode(symbolDocument.querySelector('symbol'), true);
		// container.appendChild(symbolNode);
	}

	var initialized = false;
	var busy = false;
	var shown = false;
	var loaded = false;
	var iframeResizeInterval = 1000;
	var iframeResizeIntervalId;
	var iframeUnnotifiedLoadingTimeout = 1200;
	var iframeUnnotifiedLoadingTimeoutId;
	var iframeLoadingTooLong = false;
	var iframePreviousHeight;

	// Значения по умолчанию
	var defaults = {
		type: 'load', // message, confirm, html, load
		url: null,
		content: 'Модальное окно',
		callback: undefined,
	};

	var options = extend({}, defaults);

	// DOM-элементы
	var dom = {
		root: null,
		clip: null,
		box: null,
		content: null,
		close: null,
		iframe: null,
	};
		
	//	██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████ ███████
	//	██ ████   ██ ██    ██    ██ ██   ██ ██      ██    ███  ██
	//	██ ██ ██  ██ ██    ██    ██ ███████ ██      ██   ███   █████
	//	██ ██  ██ ██ ██    ██    ██ ██   ██ ██      ██  ███    ██
	//	██ ██   ████ ██    ██    ██ ██   ██ ███████ ██ ███████ ███████
	// 
	//	#initialize
	function initialize() {

		injectSvgSymbol(iconCross);

		// DOM-элементы
		dom.root = document.querySelector(config.name.selector('modal'));
		if (!dom.root) {
			throw new Error('Для модального окна не найдены элементы в DOM');
		}
		dom.clip = dom.root.querySelector(config.name.selector('modal-clip'));
		dom.box = dom.root.querySelector(config.name.selector('modal-box'));
		dom.content = dom.root.querySelector(config.name.selector('modal-content'));
		dom.close = dom.root.querySelector(config.name.selector('modal-close'));
		dom.iframe = dom.content.querySelector('iframe');
		
		// Если включена анимация
		{
			dom.clip.style.backgroundColor = 'transparent';
			dom.box.style.opacity = 0;
			dom.box.style.transform = 'translateY(3rem)';
			// dom.box.style.transform = 'translateY(3rem) scale(.8)';
			// dom.box.style.transform = 'scale(.5)';
			dom.iframe.style.opacity = 0;
		}
		
		// Привязка событий
		dom.root.addEventListener('click', function(event) {
			if (event.target.closest(config.name.selector('modal-box')) == null) {
				hide();
			}
		}, false);
		dom.close.addEventListener('click', function(event) {
			event.preventDefault();
			hide();
		}, false);
		
		initialized = true;
	}

	//	███████ ██   ██  ██████  ██     ██
	//	██      ██   ██ ██    ██ ██     ██
	//	███████ ███████ ██    ██ ██  █  ██
	//	     ██ ██   ██ ██    ██ ██ ███ ██
	//	███████ ██   ██  ██████   ███ ███
	// 
	// 	#show
	function show(newOptions) {
		if (!initialized || busy || shown) return;
		busy = true;
		options = extend(options, newOptions);
		
		// Скрыть прокрутку страницы
		crop();

		if (options.tight) {
			setWidth('tight');
		}
		
		// Загрузка начального URL
		dom.iframe.src = options.url;
			
		// Только при первой загрузке
		dom.iframe.addEventListener('load', onFirstIframeLoad, false);

		// При каждой загрузке подгонять размер и включать/выключать отслеживание изменения размера
		dom.iframe.addEventListener('load', onEachIframeLoad, false);
			
		// Показ индикатора загрузки, если запрошенный URL загружается слишком долго 
		// (дольше iframeUnnotifiedLoadingTimeout)
		iframeUnnotifiedLoadingTimeoutId = window.setTimeout(function() {
			iframeLoadingTooLong = true;
			Spinner.spin(dom.box);
		}, iframeUnnotifiedLoadingTimeout);
		
		// Показ диалогового окна
		dom.root.style.display = null;

		{
			document.body.getBoundingClientRect(); // #reflow
			dom.clip.style.backgroundColor = null;
			window.setTimeout(function() {
				dom.box.style.opacity = null;
				dom.box.style.transform = null;
				window.setTimeout(finishShow, 400);
			}, 300);
		}
	}
	function finishShow() {
		busy = false;
		shown = true;
		document.addEventListener('keydown', escapeKeyHitHandler, false);
		if (typeof options.callback == 'function') {
			options.callback.call();
		}
	}
	function onFirstIframeLoad() {
		console.log('onFirstIframeLoad()');
		loaded = true;
		if (iframeLoadingTooLong) {
			iframeLoadingTooLong = false;
			Spinner.stop();
		} else {
			window.clearTimeout(iframeUnnotifiedLoadingTimeoutId);
		}
		
		// Показ iframe
		dom.iframe.style.opacity = null;
		// dom.iframe.css('opacity', 0).transition({opacity: 1}, 500, 'linear');
		
		// Отключение этого обработчика
		dom.iframe.removeEventListener('load', onFirstIframeLoad, false);
	}function onEachIframeLoad() {
		console.log('onEachIframeLoad()');
		iframeResize();
		iframeResizeIntervalId = window.setInterval(
			iframeResize,
			iframeResizeInterval
		);
		
		// При выгрузке iframe отключать отслеживание
		dom.iframe.addEventListener('unload', function() {
			console.log('unload');
			window.clearInterval(iframeResizeIntervalId);
		}, false);
	}function iframeResize() {
		// console.log('iframeResize()');
		var h1 = dom.iframe.contentWindow.document.documentElement.getBoundingClientRect().height;
		// var h1 = $(dom.iframe.get(0).contentWindow.document.documentElement).height();
		var h2 = marginBoxHeight(dom.iframe.contentWindow.document.body, dom.iframe.contentWindow);
		var height = Math.max(h1, h2);
		// console.log('iframe check height');
		// console.log('height = ' + height + ', previousHeight = ' + iframePreviousHeight);
		// console.log('h1 = ' + h1 + ', h2 = ' + h2);
		if (height != iframePreviousHeight) {
			var heightUnits = rem(height, true);
			iframePreviousHeight = height;
			dom.iframe.style.height = heightUnits;
			dom.box.style.height = heightUnits;
			// dom.iframe.transition({ height: height + 'px' }, 500, 'ease');
			// console.log($().jquery);
			// console.log($.fn.transition);
		}
	}
	//	██   ██ ██ ██████  ███████
	//	██   ██ ██ ██   ██ ██
	//	███████ ██ ██   ██ █████
	//	██   ██ ██ ██   ██ ██
	//	██   ██ ██ ██████  ███████
	//
	//  #hide
	function hide() {
		if (initialized && !busy && shown) {
			busy = true;
			if (loaded) {
				loaded = false;
				 
				// Убрать интервал вычисления высоты iframe
				window.clearInterval(iframeResizeIntervalId);
			} else {
				if (iframeLoadingTooLong) {
					iframeLoadingTooLong = false;
					Spinner.stop();
				} else {
					window.clearTimeout(iframeUnnotifiedLoadingTimeoutId);
				}
			}
			 
			// Скрытие диалогового окна (анимация и прочее)
			{
				// dom.box.style.transform = 'translateY(3rem)';
				// dom.box.style.transform = 'translateY(3rem) scale(.8)';
				dom.box.style.transform = 'scale(.8)';
				dom.box.style.opacity = 0;
				// window.setTimeout(function() {
				// }, 50);
				window.setTimeout(function() {
					dom.clip.style.backgroundColor = 'transparent';
				}, 300);
				window.setTimeout(finishHide, 400);

				// dom.box.transition({
				// 	transform: 'scale(.5)',
				// 	duration: transitionDuration,
				// 	easing: 'cubic-bezier(0.62,-0.5,1,0.33)',
				// 	queue: false,
				// });
				// window.setTimeout(function() {
				// 	dom.box.animate({ opacity: 0 }, boxFadeDuration);
				// }, boxFadeDelay);
				// dom.clip.transition({
				// 	opacity: 0,
				// 	delay: clipHideDelay,
				// 	duration: transitionDuration,
				// 	complete: finishHide,
				// });
			}
		}
	}function finishHide() {
		dom.root.style.display = 'none';
		busy = false;
		shown = false;
		dom.iframe.removeEventListener('load', onFirstIframeLoad, false);
		dom.iframe.removeEventListener('load', onEachIframeLoad, false);
		dom.iframe.src = 'about:blank';
		dom.iframe.style = null;
		document.removeEventListener('keydown', escapeKeyHitHandler, false);
		restore();
		iframePreviousHeight = 0;
		 
		// Перезагрузка основного окна
		// Использование forcedReload позволяет предотвратить повторную
		// загрузку iframe по последнему URL при перезагрузке страницы
		window.location.reload(true);
	} 
	//	███████ ███████ ████████
	//	██      ██         ██
	//	███████ █████      ██
	//	     ██ ██         ██
	//	███████ ███████    ██
	//
	//	██     ██ ██ ██████  ████████ ██   ██
	//	██     ██ ██ ██   ██    ██    ██   ██
	//	██  █  ██ ██ ██   ██    ██    ███████
	//	██ ███ ██ ██ ██   ██    ██    ██   ██
	//	 ███ ███  ██ ██████     ██    ██   ██
	//
	//	#set #width
	function setWidth(value) {
		value = value || 600;
		if (typeof value == 'string' || value instanceof String) {
			switch (value) {
				case 'tight':
					value = 350;
					break;
				default:
					value = 600;
					break;
			}
		}
		var remValue = rem(value, true);
		dom.box.style.width = remValue;
		dom.iframe.style.width = remValue;
	}

	// Отлавливание нажатия `Esc` и закрытие диалога
	function escapeKeyHitHandler(event) {
		// console.log('modal.js: ' + event.key);
		if ((event.key ? event.key == 'Escape' : event.keyCode == 27) && shown) {
			hide();
		}
	}

	exports.dom = dom;
	exports.initialize = initialize;
	exports.show = show;
	exports.hide = hide;
	exports.setWidth = setWidth;

	return exports;

}({}, egml.adminLayer.config));
