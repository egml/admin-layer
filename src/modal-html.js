import template from './html/modal.js';
import config from './config.js';

var data = {
	parentFontSize: config.rootFontSize,
};
var domFragment = document.createRange().createContextualFragment(template(data));
// #CBNOTE: Старые браузеры, такие как WebView 33, не поддерживают createContextualFragment (или что-то в этом духе), в общем, если ругаются, то вот так можно обойти:
// var parser = new DOMParser();
// var modalDom = parser.parseFromString(html(data), 'text/html');
// var domFragment = document.importNode(modalDom.documentElement, true);

// Встраивание HTML модального окна в DOM
document.body.appendChild(domFragment);