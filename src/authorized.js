import * as panel from './panel.js';
import * as editableArea from './editable-area.js';
import config from './config.js';

export { default as config } from './config.js';
export { setupInnerDocument } from './utils.js';
export { panel };
export { editableArea };

document.addEventListener('DOMContentLoaded', function() {
	config.global.modal.initialize();
	panel.initialize();
});
