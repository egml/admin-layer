// -------------------------------------------
//  Изменены только атрибуты генерируемого svg-тега
// -------------------------------------------
import BrowserSprite from 'svg-baker-runtime/src/browser-sprite';
import domready from 'domready';
import pkg from '../package.json';
import Name from '@egml/utils/Name';

const name = new Name(pkg.egml.name, pkg.egml.namespace);

const spriteNodeId = name.css('__SVG_SPRITE_NODE__');
const spriteGlobalVarName = name.camel('__SVG_SPRITE__');
const isSpriteExists = !!window[spriteGlobalVarName];

// eslint-disable-next-line import/no-mutable-exports
let sprite;

if (isSpriteExists) {
	sprite = window[spriteGlobalVarName];
} else {
	sprite = new BrowserSprite({
		attrs: {
			id: spriteNodeId,
			style: 'display:none;',
		}
	});
	window[spriteGlobalVarName] = sprite;
}

const loadSprite = () => {
	/**
	 * Check for page already contains sprite node
	 * If found - attach to and reuse it's content
	 * If not - render and mount the new sprite
	 */
	const existing = document.getElementById(spriteNodeId);

	if (existing) {
		sprite.attach(existing);
	} else {
		sprite.mount(document.body, true);
	}
};

if (document.body) {
	loadSprite();
} else {
	domready(loadSprite);
}

export default sprite;