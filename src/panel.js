import config from './config.js';
import iconCog from './img/icon/cog_stroke.svg';
import iconToggle from './img/icon/editable_area_toggle.svg';
import iconCross from './img/icon/cross_shape.svg';
import { injectSvgSymbol } from '@egml/utils';

var dom = {
	self: null,
};
var cssClass = {
	self: config.name.css('panel'),
	button: config.name.css('panel-button'),
};
var selector = {
	self: '.' + cssClass.self,
	button: '.' + cssClass.button,
};

//	██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████ ███████
//	██ ████   ██ ██    ██    ██ ██   ██ ██      ██    ███  ██
//	██ ██ ██  ██ ██    ██    ██ ███████ ██      ██   ███   █████
//	██ ██  ██ ██ ██    ██    ██ ██   ██ ██      ██  ███    ██
//	██ ██   ████ ██    ██    ██ ██   ██ ███████ ██ ███████ ███████
// 
// 	#initialize

export function initialize() {
	injectSvgSymbol(iconCog);
	injectSvgSymbol(iconToggle);
	injectSvgSymbol(iconCross);
	dom.self = document.querySelector(selector.self);
	if (dom.self === null) {
		throw new Error('Для панели быстрого доступа не найдены элементы в DOM');
	}
	dom.buttons = dom.self.querySelectorAll(selector.button);
	dom.buttons.forEach(function(element, id, list) {
		new Button(element);
	});
}

//	██   ██ ██ ██████  ███████
//	██   ██ ██ ██   ██ ██
//	███████ ██ ██   ██ █████
//	██   ██ ██ ██   ██ ██
//	██   ██ ██ ██████  ███████
// 
//	#hide

export function hide() {
	document.querySelector(config.name.cssClassSelector('panel')).classList.add(config.name.css('panel--hidden'));
}

//	██████  ██    ██ ████████ ████████  ██████  ███    ██
//	██   ██ ██    ██    ██       ██    ██    ██ ████   ██
//	██████  ██    ██    ██       ██    ██    ██ ██ ██  ██
//	██   ██ ██    ██    ██       ██    ██    ██ ██  ██ ██
//	██████   ██████     ██       ██     ██████  ██   ████
// 
//	#button

function Button(target) {
	var self = this;
	target.addEventListener('click', function(event) {
		event.preventDefault();
		// // var url = self.dom.self.href;
		// if (self.dom.self.getAttribute('href') != '#') {
		// 	modal.show({url: url, callback: hide});
		// }
	});
}