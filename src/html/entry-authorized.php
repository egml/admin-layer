<?php
	require __DIR__ . '/../vendor/autoload.php';

	$rootFontSize = 14;

	$templateEngine = new Pug;
	$template = file_get_contents(__DIR__ . '/../src/html/panel.pug');
	$data = [
		'url' => '#',
		'parentFontSize' => $rootFontSize,
	];
	$panel = $templateEngine->render($template, $data);

	$template = file_get_contents(__DIR__ . '/../src/html/modal.pug');
	$data = [
		'parentFontSize' => $rootFontSize,
	];
	$modal = $templateEngine->render($template, $data);

	$path = 'assets/';
	$filePrefix = '';
	if (isset($_GET['dist'])) {
		$path = '../dist/';
		$filePrefix = 'egml-admin-layer-';
	}
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Зарегистрированный - Демонстрация Admin Layer</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/demo.css">
	<link rel="stylesheet" href="<?= $path ?><?= $filePrefix ?>authorized.css">

	<?php /* ?>
	<script src="<?= $path ?><?= $filePrefix ?>common--authorized--guest.chunk.js"></script>
	<?php if (empty($filePrefix)): ?>
		<script src="<?= $path ?>common--authorized.chunk.js"></script>
	<?php endif ?>
	<script src="<?= $path ?><?= $filePrefix ?>authorized.js"></script>
	<?php */ ?>
</head>
<body>
	<script>
		egml.adminLayer.config.rootFontSize = <?= $rootFontSize ?>;
		egml.adminLayer.config.publicPath = '<?= $path ?>';
		egml.adminLayer.config.innerStyleUrl = '../assets/inner-style.css';
	
		document.addEventListener('DOMContentLoaded', function() {
			egml.adminLayer.editableArea.create({
				'selector': 'p',
				'url': 'urls/area-edit.php',
				'title': 'Заголовок',
			});
		});
	</script>
	
	<div style="margin:3rem 5rem;">
		<p>
			Кристаллическая решетка, как и везде в пределах наблюдаемой вселенной, выталкивает магнит. Волновая тень принципиально неизмерима. Исследователями из разных лабораторий неоднократно наблюдалось, как колебание ненаблюдаемо. Еще в ранних работах Л.Д.Ландау показано, что плазменное образование отталкивает наносекундный кварк при любом их взаимном расположении. Силовое поле, если рассматривать процессы в рамках специальной теории относительности, когерентно сжимает межядерный экситон, даже если пока мы не можем наблюсти это непосредственно.
		</p>
	</div>

	<?= $modal ?>
	<?= $panel ?>
</body>
</html>