export default function(data) {
	var template =
`<div class="egml-admin_layer-editable_area egml-admin_layer-editable_area--hidden" title="Настроить область" style="font-size:${ 16 / data.parentFontSize }em">
	${ data.hasTitle ? `<div class="egml-admin_layer-editable_area-title">${ data.title }</div>` : '' }
</div>`;
	return template;
}