//	 ██████  ██████  ███    ██ ███████ ██  ██████
//	██      ██    ██ ████   ██ ██      ██ ██
//	██      ██    ██ ██ ██  ██ █████   ██ ██   ███
//	██      ██    ██ ██  ██ ██ ██      ██ ██    ██
//	 ██████  ██████  ██   ████ ██      ██  ██████
//
//	#config
import Name from '@egml/utils/Name';
import pkg from '../package.json';

Name.prototype.ns = pkg.egml.namespace;
 
var config = {
	name: new Name(pkg.egml.name),
	publicPath: null,
	rootFontSize: 16,
	innerStyleUrl: null,
};
  
Object.defineProperty(config, 'global', {
	get: function() {
		return window[config.name.ns.camel][config.name.base.camel];
	}
});

export default config;