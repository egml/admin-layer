var scroll;

export function crop() {
	if (window.scrollX || window.scrollY) {
		scroll = {
			top: window.scrollY,
			left: window.scrollX,
		};
	}
	document.documentElement.style.overflow = 'hidden';
	//document.body.style.overflow = 'hidden';
};

export function restore() {
	document.documentElement.style.overflow = null;
	//document.body.style.overflow = null;
};
