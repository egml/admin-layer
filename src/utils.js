//	██    ██ ████████ ██ ██      ███████
//	██    ██    ██    ██ ██      ██
//	██    ██    ██    ██ ██      ███████
//	██    ██    ██    ██ ██           ██
//	 ██████     ██    ██ ███████ ███████
// 
//	#utils
import { injectStyle } from '@egml/utils';
import config from './config.js';
// import { hide as hideModal } from './modal';

//	███████ ███████ ████████ ██    ██ ██████
//	██      ██         ██    ██    ██ ██   ██
//	███████ █████      ██    ██    ██ ██████
//	     ██ ██         ██    ██    ██ ██
//	███████ ███████    ██     ██████  ██
// 
//	██ ███    ██ ███    ██ ███████ ██████
//	██ ████   ██ ████   ██ ██      ██   ██
//	██ ██ ██  ██ ██ ██  ██ █████   ██████
//	██ ██  ██ ██ ██  ██ ██ ██      ██   ██
//	██ ██   ████ ██   ████ ███████ ██   ██
// 
//	██████   ██████   ██████ ██    ██ ███    ███ ███████ ███    ██ ████████
//	██   ██ ██    ██ ██      ██    ██ ████  ████ ██      ████   ██    ██
//	██   ██ ██    ██ ██      ██    ██ ██ ████ ██ █████   ██ ██  ██    ██
//	██   ██ ██    ██ ██      ██    ██ ██  ██  ██ ██      ██  ██ ██    ██
//	██████   ██████   ██████  ██████  ██      ██ ███████ ██   ████    ██
// 
//	#setup #inner #document
export function setupInnerDocument(innerDocument) {
	// При нажатии `Esc` закрывать окно слоя наложения
	innerDocument.addEventListener('keydown', function(event) {
		if (event.key ? event.key == 'Escape' : event.keyCode == 27) {
			config.global.modal.hide();
		}
	});
	 
	// Встраивание стиля во внутренний документ
	if (config.innerStyleUrl) {
		injectStyle({
			url: config.innerStyleUrl,
			targetDocument: innerDocument,
		});
	}
}