import Spinner from './spinner.js';
import * as ViewportCropper from './viewport-cropper.js';
import iconCross from './img/icon/cross.svg';
import config from './config.js';
import { extend, rem, marginBoxHeight, injectSvgSymbol } from '@egml/utils';

var initialized = false;
var busy = false;
var shown = false;
var loaded = false;
var iframeResizeInterval = 1000;
var iframeResizeIntervalId;
var iframeUnnotifiedLoadingTimeout = 1200;
var iframeUnnotifiedLoadingTimeoutId;
var iframeLoadingTooLong = false;
var iframePreviousHeight;
var transitionDuration = 400;
var clipHideDelay = transitionDuration - transitionDuration * 0.25
var boxShowDelay = clipHideDelay;
var boxFadeDelay = transitionDuration * 0.8;
var boxFadeDuration = transitionDuration - boxFadeDelay;
var animate = true;

// Значения по умолчанию
var defaults = {
	type: 'load', // message, confirm, html, load
	url: null,
	content: 'Модальное окно',
	callback: undefined,
};

var options = extend({}, defaults);

// DOM-элементы
export var dom = {
	root: null,
	clip: null,
	box: null,
	content: null,
	close: null,
	iframe: null,
};
	
//	██ ███    ██ ██ ████████ ██  █████  ██      ██ ███████ ███████
//	██ ████   ██ ██    ██    ██ ██   ██ ██      ██    ███  ██
//	██ ██ ██  ██ ██    ██    ██ ███████ ██      ██   ███   █████
//	██ ██  ██ ██ ██    ██    ██ ██   ██ ██      ██  ███    ██
//	██ ██   ████ ██    ██    ██ ██   ██ ███████ ██ ███████ ███████
// 
//	#initialize
export function initialize() {

	injectSvgSymbol(iconCross);

	// DOM-элементы
	dom.root = document.querySelector(config.name.selector('modal'));
	if (!dom.root) {
		throw new Error('Для модального окна не найдены элементы в DOM');
	}
	dom.clip = dom.root.querySelector(config.name.selector('modal-clip'));
	dom.box = dom.root.querySelector(config.name.selector('modal-box'));
	dom.content = dom.root.querySelector(config.name.selector('modal-content'));
	dom.close = dom.root.querySelector(config.name.selector('modal-close'));
	dom.iframe = dom.content.querySelector('iframe');
	
	// Если включена анимация
	if (animate) {
		dom.clip.style.backgroundColor = 'transparent';
		dom.box.style.opacity = 0;
		dom.box.style.transform = 'translateY(3rem)';
		// dom.box.style.transform = 'translateY(3rem) scale(.8)';
		// dom.box.style.transform = 'scale(.5)';
		dom.iframe.style.opacity = 0;
	}
	
	// Привязка событий
	dom.root.addEventListener('click', function(event) {
		if (event.target.closest(config.name.selector('modal-box')) == null) {
			hide();
		}
	}, false);
	dom.close.addEventListener('click', function(event) {
		event.preventDefault();
		hide();
	}, false);
	
	initialized = true;
}

//	███████ ██   ██  ██████  ██     ██
//	██      ██   ██ ██    ██ ██     ██
//	███████ ███████ ██    ██ ██  █  ██
//	     ██ ██   ██ ██    ██ ██ ███ ██
//	███████ ██   ██  ██████   ███ ███
// 
// 	#show
export function show(newOptions) {
	if (!initialized || busy || shown) return;
	busy = true;
	options = extend(options, newOptions);
	
	// Скрыть прокрутку страницы
	ViewportCropper.crop();

	if (options.tight) {
		setWidth('tight');
	}
	
	// Загрузка начального URL
	dom.iframe.src = options.url;
		
	// Только при первой загрузке
	dom.iframe.addEventListener('load', onFirstIframeLoad, false);

	// При каждой загрузке подгонять размер и включать/выключать отслеживание изменения размера
	dom.iframe.addEventListener('load', onEachIframeLoad, false);
		
	// Показ индикатора загрузки, если запрошенный URL загружается слишком долго 
	// (дольше iframeUnnotifiedLoadingTimeout)
	iframeUnnotifiedLoadingTimeoutId = window.setTimeout(function() {
		iframeLoadingTooLong = true;
		Spinner.spin(dom.box);
	}, iframeUnnotifiedLoadingTimeout);
	
	// Показ диалогового окна
	dom.root.style.display = null;

	if (!animate) {
		finishShow();
	} else {
		document.body.getBoundingClientRect(); // #reflow
		dom.clip.style.backgroundColor = null;
		window.setTimeout(function() {
			dom.box.style.opacity = null;
			dom.box.style.transform = null;
			window.setTimeout(finishShow, 400);
		}, 300);
	}
};

function finishShow() {
	busy = false;
	shown = true;
	document.addEventListener('keydown', escapeKeyHitHandler, false);
	if (typeof options.callback == 'function') {
		options.callback.call();
	}
};

function onFirstIframeLoad() {
	console.log('onFirstIframeLoad()');
	loaded = true;
	if (iframeLoadingTooLong) {
		iframeLoadingTooLong = false;
		Spinner.stop();
	} else {
		window.clearTimeout(iframeUnnotifiedLoadingTimeoutId);
	}
	
	// Показ iframe
	dom.iframe.style.opacity = null;
	// dom.iframe.css('opacity', 0).transition({opacity: 1}, 500, 'linear');
	
	// Отключение этого обработчика
	dom.iframe.removeEventListener('load', onFirstIframeLoad, false);
};
function onEachIframeLoad() {
	console.log('onEachIframeLoad()');
	iframeResize();
	iframeResizeIntervalId = window.setInterval(
		iframeResize,
		iframeResizeInterval
	);
	
	// При выгрузке iframe отключать отслеживание
	dom.iframe.addEventListener('unload', function() {
		console.log('unload');
		window.clearInterval(iframeResizeIntervalId);
	}, false);
};
function iframeResize() {
	// console.log('iframeResize()');
	var h1 = dom.iframe.contentWindow.document.documentElement.getBoundingClientRect().height;
	// var h1 = $(dom.iframe.get(0).contentWindow.document.documentElement).height();
	var h2 = marginBoxHeight(dom.iframe.contentWindow.document.body, dom.iframe.contentWindow);
	var height = Math.max(h1, h2);
	// console.log('iframe check height');
	// console.log('height = ' + height + ', previousHeight = ' + iframePreviousHeight);
	// console.log('h1 = ' + h1 + ', h2 = ' + h2);
	if (height != iframePreviousHeight) {
		var heightUnits = rem(height, true);
		iframePreviousHeight = height;
		dom.iframe.style.height = heightUnits;
		dom.box.style.height = heightUnits;
		// dom.iframe.transition({ height: height + 'px' }, 500, 'ease');
		// console.log($().jquery);
		// console.log($.fn.transition);
	}
};

//	██   ██ ██ ██████  ███████
//	██   ██ ██ ██   ██ ██
//	███████ ██ ██   ██ █████
//	██   ██ ██ ██   ██ ██
//	██   ██ ██ ██████  ███████
//
//  #hide
export function hide() {
	if (initialized && !busy && shown) {
		busy = true;
		if (loaded) {
			loaded = false;
			 
			// Убрать интервал вычисления высоты iframe
			window.clearInterval(iframeResizeIntervalId);
		} else {
			if (iframeLoadingTooLong) {
				iframeLoadingTooLong = false;
				Spinner.stop();
			} else {
				window.clearTimeout(iframeUnnotifiedLoadingTimeoutId);
			}
		}
		 
		// Скрытие диалогового окна (анимация и прочее)
		if (!animate) {
			finishHide();
		} else {
			// dom.box.style.transform = 'translateY(3rem)';
			// dom.box.style.transform = 'translateY(3rem) scale(.8)';
			dom.box.style.transform = 'scale(.8)';
			dom.box.style.opacity = 0;
			// window.setTimeout(function() {
			// }, 50);
			window.setTimeout(function() {
				dom.clip.style.backgroundColor = 'transparent';
			}, 300);
			window.setTimeout(finishHide, 400);

			// dom.box.transition({
			// 	transform: 'scale(.5)',
			// 	duration: transitionDuration,
			// 	easing: 'cubic-bezier(0.62,-0.5,1,0.33)',
			// 	queue: false,
			// });
			// window.setTimeout(function() {
			// 	dom.box.animate({ opacity: 0 }, boxFadeDuration);
			// }, boxFadeDelay);
			// dom.clip.transition({
			// 	opacity: 0,
			// 	delay: clipHideDelay,
			// 	duration: transitionDuration,
			// 	complete: finishHide,
			// });
		}
	}
};
function finishHide() {
	dom.root.style.display = 'none';
	busy = false;
	shown = false;
	dom.iframe.removeEventListener('load', onFirstIframeLoad, false);
	dom.iframe.removeEventListener('load', onEachIframeLoad, false);
	dom.iframe.src = 'about:blank';
	dom.iframe.style = null;
	document.removeEventListener('keydown', escapeKeyHitHandler, false);
	ViewportCropper.restore();
	iframePreviousHeight = 0;
	 
	// Перезагрузка основного окна
	// Использование forcedReload позволяет предотвратить повторную
	// загрузку iframe по последнему URL при перезагрузке страницы
	window.location.reload(true);
};
 
//	███████ ███████ ████████
//	██      ██         ██
//	███████ █████      ██
//	     ██ ██         ██
//	███████ ███████    ██
//
//	██     ██ ██ ██████  ████████ ██   ██
//	██     ██ ██ ██   ██    ██    ██   ██
//	██  █  ██ ██ ██   ██    ██    ███████
//	██ ███ ██ ██ ██   ██    ██    ██   ██
//	 ███ ███  ██ ██████     ██    ██   ██
//
//	#set #width
export function setWidth(value) {
	value = value || 600;
	if (typeof value == 'string' || value instanceof String) {
		switch (value) {
			case 'tight':
				value = 350;
				break;
			default:
				value = 600;
				break;
		}
	}
	var remValue = rem(value, true);
	dom.box.style.width = remValue;
	dom.iframe.style.width = remValue;
}

// Отлавливание нажатия `Esc` и закрытие диалога
function escapeKeyHitHandler(event) {
	// console.log('modal.js: ' + event.key);
	if ((event.key ? event.key == 'Escape' : event.keyCode == 27) && shown) {
		hide();
	}
}