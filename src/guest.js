import { injectScript, injectStyle, injectSvgSymbol } from '@egml/utils';
import config from './config.js';
import iconLock from './img/icon/lock.svg';

export { default as config } from './config.js';
export { setupInnerDocument } from './utils.js';

var busy = false;
var loaded = false;

document.addEventListener('DOMContentLoaded', function() {
	injectSvgSymbol(iconLock);
	var accessPoints = document.querySelectorAll(config.name.cssClassSelector('access_point'));
	if (accessPoints.length) {
		for (var i = 0; i < accessPoints.length; i++) {
			accessPoints[i].addEventListener('click', function(event) {
				if (busy || loaded) return;
				event.preventDefault();
				var accessPoint = this;
				var classLoading = config.name.css('access_point--loading');
				busy = true;
				accessPoint.classList.add(classLoading);
				injectStyle(config.publicPath + config.name.kebab('modal.css'), function() {
					injectScript(config.publicPath + config.name.kebab('modal-html.js'), function() {
						injectScript(config.publicPath + config.name.kebab('modal.js'), function() {
							loaded = true;
							accessPoint.classList.remove(classLoading);
							config.global.modal.initialize();
							config.global.modal.show({ url: accessPoint.href });
						});
					});
				});
			}, false);
		}
	}
});