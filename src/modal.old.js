import $ from 'jquery';
import 'jquery.transit';
import Spinner from './spinner';
import * as ViewportCropper from './viewport-cropper';
import Utils from './utils';
import '../img/svg-sprite/cross.svg';
import config from './config';
import * as utils from '@egml/utils';

var initialized = false;
var busy = false;
var shown = false;
var loaded = false;
var iframeResizeInterval = 1000;
var iframeResizeIntervalId;
var iframeUnnotifiedLoadingTimeout = 1200;
var iframeUnnotifiedLoadingTimeoutId;
var iframeLoadingTooLong = false;
var iframePreviousHeight;
var transitionDuration = 400;
var overlayHideDelay = transitionDuration - transitionDuration * 0.25
var boxShowDelay = overlayHideDelay;
var boxFadeDelay = transitionDuration * 0.8;
var boxFadeDuration = transitionDuration - boxFadeDelay;
var animate = true;
// var animate = false;

// Значения по умолчанию
var defaults = {
	type: 'load', // message, confirm, html, load
	url: null,
	content: 'Модальное окно',
	callback: undefined,
};

var options = {};

// DOM-элементы
var doc;
export var root, overlay, box, content, close, iframe;
	
//	██ ███    ██ ██ ████████
//	██ ████   ██ ██    ██
//	██ ██ ██  ██ ██    ██
//	██ ██  ██ ██ ██    ██
//	██ ██   ████ ██    ██
// 
// #init

export function initialize() {
	
	// DOM-элементы
	root = $('.egml-admin_layer-modal').eq(0);
	if (root.length == 0) {
		throw new Error('Для модального окна не найдены элементы в DOM');
	}
	doc = $(document);
	overlay = root.find('.egml-admin_layer-modal-overlay');
	box = root.find('.egml-admin_layer-modal-box');
	content = root.find('.egml-admin_layer-modal-content');
	close = root.find('.egml-admin_layer-modal-close');
	iframe = content.find('iframe');
	
	// Для анимации
	if (animate) {
		overlay.css('opacity', 0);
		box.css({
			opacity: 0,
			transform: 'scale(.5)',
		});
	}
	
	// Привязка событий
	close.on('click', function(e) {
		e.preventDefault();
		hide();
	});
	// overlay.on('click', hide);

	root.get(0).addEventListener('click', function(event) {
		if (event.target.closest('.egml-admin_layer-modal-box') == null) {
			hide();
		}
	}, false);
	
	initialized = true;
}

// /*
// ███████ ██   ██  ██████  ██     ██
// ██      ██   ██ ██    ██ ██     ██
// ███████ ███████ ██    ██ ██  █  ██
//      ██ ██   ██ ██    ██ ██ ███ ██
// ███████ ██   ██  ██████   ███ ███
// */

// Показ диалогового окна
export function show(newOptions) {
	if (!initialized || busy || shown) return;
	busy = true;
	options = $.extend(defaults, newOptions);
	
	// Скрыть прокрутку страницы
	ViewportCropper.crop();

	if (options.tight) {
		setTight();
	}
	
	iframe
		// Подготовка iframe для анимации
		.css('opacity', 0)
	
		// Загрузка начального URL
		.prop('src', options.url)
		
		// Логика при каждой загрузке: подгонять размер и включать/выключать отслеживание изменения размера
		.on('load', iframeOnEachLoadHandler)
		
		// Логика при первой загрузке
		.on('load', iframeOnFirstLoadHandler)
	;
	
	// Показ индикатора загрузки, если запрошенный URL загружается слишком долго (дольше iframeUnnotifiedLoadingTimeout)
	iframeUnnotifiedLoadingTimeoutId = window.setTimeout(function() {
		// console.log('iframeLoadingTooLong');
		iframeLoadingTooLong = true;
		// Индиктор загрузки
		Spinner.spin(box.get(0));
		// Искусственная отсрочка показа, чтобы не было "дерганья", когда iframe загрузился сразу после показа индикатора загрузки
		// window.setTimeout(function() {
			// iframeLoadingTooLong = false;
			// if (loaded) {
			// 	Spinner.stop();
			// 	iframeOnFirstLoadHandler();
			// }
		// }, iframeUnnotifiedLoadingTimeout);
	}, iframeUnnotifiedLoadingTimeout);
	
	// Показ диалогового окна
	root.show();
	if (!animate) {
		finishShow();
	} else {
		document.body.getBoundingClientRect(); // reflow
		overlay.transition({
			opacity: 1,
			duration: transitionDuration,
		});
		window.setTimeout(function() {
			// FIXME: animate в данном случае используется как костыль, т.к. transition не позволяет анимировать несколько свойств с разной длительностью (?: или... попробовать несколько transition подряд UPD: транзишены, вероятно, выполняются последовательно, собираются в очередь)
			box
			.animate({ opacity: 1 }, boxFadeDuration)
			.transition({
				transform: 'scale(1)',
				duration: transitionDuration,
				easing: 'cubic-bezier(0,0.45,0.6,1.39)',
				complete: finishShow,
				queue: false,
			});
		}, boxShowDelay);
	}
};

function finishShow() {
	busy = false;
	shown = true;
	doc.on('keydown', escapeKeyHitHandler);
	if (typeof options.callback == 'function') {
		options.callback.call();
	}
};

// /*
// ██   ██ ██ ██████  ███████
// ██   ██ ██ ██   ██ ██
// ███████ ██ ██   ██ █████
// ██   ██ ██ ██   ██ ██
// ██   ██ ██ ██████  ███████
// */

// Закрытие диалога
export function hide() {
	if (initialized && !busy && shown) {
		busy = true;
		if (loaded) {
			loaded = false;
			
			// Убрать интервал вычисления высоты iframe
			window.clearInterval(iframeResizeIntervalId);
		} else {
			if (iframeLoadingTooLong) {
				iframeLoadingTooLong = false;
				Spinner.stop();
			} else {
				window.clearTimeout(iframeUnnotifiedLoadingTimeoutId);
			}
		}
		
		// Скрытие диалогового окна (анимация и прочее)
		if (!animate) {
			finishHide();
		} else {
			box.transition({
				transform: 'scale(.5)',
				duration: transitionDuration,
				easing: 'cubic-bezier(0.62,-0.5,1,0.33)',
				queue: false,
			});
			window.setTimeout(function() {
				box.animate({ opacity: 0 }, boxFadeDuration);
			}, boxFadeDelay);
			overlay.transition({
				opacity: 0,
				delay: overlayHideDelay,
				duration: transitionDuration,
				complete: finishHide,
			});
		}
	}
};
function finishHide() {
	root.hide();
	busy = false;
	shown = false;
	iframe
		.off('load', iframeOnFirstLoadHandler)
		.off('load', iframeOnEachLoadHandler)
		.prop('src', 'about:blank')
		.get(0).style = null;
	doc.off('keydown', escapeKeyHitHandler);
	ViewportCropper.restore();
	iframePreviousHeight = 0;
	
	// Перезагрузка основного окна
	// Использование forcedReload позволяет предотвратить повторной 
	// загрузки iframe по последнему URL при перезагрузке страницы
	window.location.reload(true);
};

export function setWidth(value) {
	value = value || 600;
	if (typeof value == 'string') {
		switch (value) {
			case 'tight':
				value = 350;
				break;
			default:
				value = 600;
				break;
		}
	}
	box.get(0).style.width = utils.rem(value);
}

export function setTight() {
	box.get(0).classList.add(config.name.css('modal-box--tight'));
}

export function setDefaultWidth() {
	box.get(0).classList.remove(config.name.css('modal-box--tight'));
}

export function setupInnerDocument(innerDocument) {
	// При нажатии `Esc` закрывать окно слоя наложения
	innerDocument.addEventListener('keydown', function(event) {
		if (event.key ? event.key == 'Escape' : event.keyCode == 27) {
			hide();
		}
	});
	
	// Встраивание стиля во внутренний документ
	if (config.innerStyleUrl) {
		utils.injectStyle(config.innerStyleUrl, false, innerDocument);
	}
}

// /*
// ██    ██  █████  ██████
// ██    ██ ██   ██ ██   ██
// ██    ██ ███████ ██████
//  ██  ██  ██   ██ ██   ██
//   ████   ██   ██ ██   ██
// */

function iframeOnFirstLoadHandler() {
	// console.log('iframeOnFirstLoadHandler()');
	loaded = true;
	if (iframeLoadingTooLong) {
		iframeLoadingTooLong = false;
		Spinner.stop();
	} else {
		window.clearTimeout(iframeUnnotifiedLoadingTimeoutId);
	}
	
	// NOTE: Транзишены ставятся в очередь, они не происходят одновременно, поэтому далее показ и изменение размера iframe происходят последовательно, а не одновременно
	
	// Показ iframe
	iframe.css('opacity', 0).transition({opacity: 1}, 500, 'linear');
	
	// Отключение этого обработчика
	iframe.off('load', iframeOnFirstLoadHandler);
};
function iframeOnEachLoadHandler() {
	// console.log('iframeOnEachLoadHandler()');
	iframeResize();
	iframeResizeIntervalId = window.setInterval(
		iframeResize,
		iframeResizeInterval
	);
	
	// При выгрузке iframe отключать отслеживание
	$(iframe.get(0).contentWindow).on('unload', function() {
		// console.log('iframe "unload"');
		window.clearInterval(iframeResizeIntervalId);
	});
};
function iframeResize() {
	// console.log('iframeResize()');
	var h1 = $(iframe.get(0).contentWindow.document.documentElement).height();
	var h2 = Utils.marginBoxHeight(iframe.get(0).contentWindow.document.body, iframe.get(0).contentWindow);
	var height = Math.max(
		h1,
		h2
	);
	// console.log('iframe check height');
	// console.log('height = ' + height + ', previousHeight = ' + iframePreviousHeight);
	// console.log('h1 = ' + h1 + ', h2 = ' + h2);
	if (height != iframePreviousHeight) {
		iframePreviousHeight = height;
		iframe.transition({ height: height + 'px' }, 500, 'ease');
		// console.log($().jquery);
		// console.log($.fn.transition);
	}
};
// Отлавливание нажатия `Esc` и закрытие диалога
function escapeKeyHitHandler(event) {
	// console.log('modal.js: ' + event.key);
	if ((event.key ? event.key == 'Escape' : event.keyCode == 27) && shown) {
		hide();
	}
}