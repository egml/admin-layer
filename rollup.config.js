import path from 'path';
import nodeResolvePlugin from 'rollup-plugin-node-resolve';
import commonjsPlugin from 'rollup-plugin-commonjs';
import jsonPlugin from 'rollup-plugin-json';
import svgToSymbolPlugin from 'rollup-plugin-svg-to-symbol';
// import sassPlugin from 'rollup-plugin-sass';
// import nodeSass from 'node-sass';
// import progressPlugin from 'rollup-plugin-progress';
// import { string as stringPlugin } from 'rollup-plugin-string';
// import svgoPlugin from 'rollup-plugin-svgo';

import Name from '@egml/utils/node/Name';
import pkg from './package.json';

var name = new Name(pkg.egml.name, pkg.egml.namespace);

// ID модулей для исключения в конфигурации
var moduleId = {
	config: path.resolve(__dirname, 'src/config.js'),
	modal: path.resolve(__dirname, 'src/modal.js'),
};

var svgToSymbolPluginInstance = svgToSymbolPlugin({
	extractId: function(file) {
		return name.css('svg_sprite-' + file.name);
	},
	svgo: {
		removeViewBox: false,
		removeDimensions: true,
		removeUselessStrokeAndFill: false,
		cleanupNumericValues: false,
		convertPathData: false,
		mergePaths: false,
	},
});

var jsonPluginInstance = jsonPlugin({
	compact: true,
	namedExports: false,
});

export default [
	
	//	 ██████  ██    ██ ███████ ███████ ████████
	//	██       ██    ██ ██      ██         ██
	//	██   ███ ██    ██ █████   ███████    ██
	//	██    ██ ██    ██ ██           ██    ██
	//	 ██████   ██████  ███████ ███████    ██
	// 
	//	#guest
	{
		input: 'src/guest.js',
		output: {
			file: 'demo/assets/' + name.kebab('guest.js'),
			format: 'iife',
			name: name.dot(),
			// sourcemap: 'inline',
		},
		plugins: [
			nodeResolvePlugin(),
			commonjsPlugin(),
			jsonPluginInstance,
			svgToSymbolPluginInstance,
		],
	},

	//	 █████  ██    ██ ████████ ██   ██  ██████  ██████  ██ ███████ ███████ ██████
	//	██   ██ ██    ██    ██    ██   ██ ██    ██ ██   ██ ██    ███  ██      ██   ██
	//	███████ ██    ██    ██    ███████ ██    ██ ██████  ██   ███   █████   ██   ██
	//	██   ██ ██    ██    ██    ██   ██ ██    ██ ██   ██ ██  ███    ██      ██   ██
	//	██   ██  ██████     ██    ██   ██  ██████  ██   ██ ██ ███████ ███████ ██████
	// 
	//	#authorized
	{
		input: 'src/authorized.js',
		output: {
			file: 'demo/assets/' + name.kebab('authorized.js'),
			format: 'iife',
			name: name.dot(),
			// sourcemap: 'inline',
		},
		plugins: [
			nodeResolvePlugin(),
			commonjsPlugin(),
			jsonPluginInstance,
			svgToSymbolPluginInstance,
		],
	},

	//	███    ███  ██████  ██████   █████  ██
	//	████  ████ ██    ██ ██   ██ ██   ██ ██
	//	██ ████ ██ ██    ██ ██   ██ ███████ ██
	//	██  ██  ██ ██    ██ ██   ██ ██   ██ ██
	//	██      ██  ██████  ██████  ██   ██ ███████
	// 
	//	#modal
	{
		input: 'src/modal.js',
		output: {
			file: 'demo/assets/' + name.kebab('modal.js'),
			format: 'iife',
			name: name.dot('modal'),
			globals: {
				[ moduleId.config ]: name.dot('config'),
			},
			// sourcemap: 'inline',
		},
		external: [
			moduleId.config,
		],
		plugins: [
			nodeResolvePlugin(),
			commonjsPlugin(),
			svgToSymbolPluginInstance,
		],
	},

	//	███    ███  ██████  ██████   █████  ██
	//	████  ████ ██    ██ ██   ██ ██   ██ ██
	//	██ ████ ██ ██    ██ ██   ██ ███████ ██
	//	██  ██  ██ ██    ██ ██   ██ ██   ██ ██
	//	██      ██  ██████  ██████  ██   ██ ███████
	// 
	//	██   ██ ████████ ███    ███ ██
	//	██   ██    ██    ████  ████ ██
	//	███████    ██    ██ ████ ██ ██
	//	██   ██    ██    ██  ██  ██ ██
	//	██   ██    ██    ██      ██ ███████
	// 
	//	#modal #html
	{
		input: 'src/modal-html.js',
		output: {
			file: 'demo/assets/' + name.kebab('modal-html.js'),
			format: 'iife',
			globals: {
				[ moduleId.config ]: name.dot('config'),
			},
			// sourcemap: 'inline',
		},
		external: [
			moduleId.config,
		],
		plugins: [
			commonjsPlugin(),
			nodeResolvePlugin(),
		],
	},
	
];